package fa.training.services;

import fa.training.models.dto.VaccineTypeDTO;
import fa.training.models.entity.VaccineType;
import java.util.List;
import java.util.Set;

public interface VaccineTypeService {

  Set<String> getAllVaccineTypeNames();

  List<String> getAllVaccineTypeName();

  List<VaccineTypeDTO> getAll();

  VaccineType updateById(String id);

  VaccineType save(VaccineTypeDTO vaccineTypeDTO);
}
