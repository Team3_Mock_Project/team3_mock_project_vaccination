package fa.training.services;

import fa.training.models.dto.VaccineDTO;
import fa.training.models.dto.VaccineReportDTO;
import fa.training.models.entity.Vaccine;

import java.util.List;
import java.util.Optional;
import java.util.UUID;


public interface VaccineService {

    List<Vaccine> getAll();

    List<VaccineReportDTO> getAllVaccineReport();

    List<VaccineReportDTO> getAllVaccineByFilter(String beginDate, String endDate, String vaccineType,
                                                 String origin);

    List<Integer> getNumberVaccineByMonthAndYear(Integer year);

    List<VaccineDTO> getAllVaccine();

    VaccineDTO getById(UUID id);

    Vaccine createNew(VaccineDTO vaccineDTO);

    Vaccine update(VaccineDTO vaccineDTO);

    Optional<Vaccine> findById(UUID id);

    Vaccine updateStatus(UUID id);
}
