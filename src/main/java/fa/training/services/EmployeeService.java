package fa.training.services;

import fa.training.models.entity.Employee;

public interface EmployeeService {

  Employee findAdminByUsernameAndPassword(String username, String password);
}
