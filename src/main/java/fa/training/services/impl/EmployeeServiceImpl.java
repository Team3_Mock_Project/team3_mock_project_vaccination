package fa.training.services.impl;

import fa.training.models.entity.Employee;
import fa.training.repositories.EmployeeRepository;
import fa.training.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl implements EmployeeService {

  @Autowired
  EmployeeRepository employeeRepository;

  @Override
  public Employee findAdminByUsernameAndPassword(String username, String password) {
    return employeeRepository.findAdminByUsernameAndPassword(username, password);
  }
}
