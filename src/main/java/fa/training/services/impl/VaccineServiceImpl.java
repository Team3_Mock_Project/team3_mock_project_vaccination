package fa.training.services.impl;

import fa.training.models.dto.VaccineDTO;
import fa.training.models.dto.VaccineReportDTO;
import fa.training.models.entity.Vaccine;
import fa.training.repositories.VaccineRepository;
import fa.training.repositories.VaccineTypeRepository;
import fa.training.services.VaccineService;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import fa.training.services.VaccineTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VaccineServiceImpl implements VaccineService {

    @Autowired
    final VaccineRepository vaccineRepository;

    @Autowired
    final VaccineTypeRepository vaccineTypeRepository;

    public VaccineServiceImpl(VaccineRepository vaccineRepository, VaccineTypeService vaccineTypeService, VaccineTypeRepository vaccineTypeRepository) {
        this.vaccineRepository = vaccineRepository;
        this.vaccineTypeRepository = vaccineTypeRepository;
    }

    @Override
    public List<Vaccine> getAll() {
        return vaccineRepository.findAll();
    }

    @Override
    public List<VaccineReportDTO> getAllVaccineReport() {
        List<VaccineReportDTO> vaccineReportDTOS = new ArrayList<>();

        List<Vaccine> vaccines = vaccineRepository.findAll();

        for (Vaccine vaccine : vaccines) {
            VaccineReportDTO vaccineReportDTO = VaccineReportDTO.builder()
                    .vaccineId(vaccine.getVaccineId().toString())
                    .vaccineName(vaccine.getVaccineName())
                    .vaccineType(vaccine.getVaccineTypeByVaccineTypeId().getVaccineTypeName())
                    .numOfInject(vaccine.getNumberOfInjection())
                    .beginNextInjectDate(vaccine.getTimeBeginNextInjection().toString())
                    .endNextInjectDate(vaccine.getTimeEndNextInjection().toString())
                    .origin(vaccine.getOrigin())
                    .build();
            vaccineReportDTOS.add(vaccineReportDTO);
        }
        return vaccineReportDTOS;
    }

    @Override
    public List<VaccineDTO> getAllVaccine() {
        Date currentDate = new Date(System.currentTimeMillis());
        List<VaccineDTO> vaccineDTOS = new ArrayList<>();
        List<Vaccine> vaccines = vaccineRepository.findAll();
        for (Vaccine vaccine : vaccines) {
            VaccineDTO vaccineDTO = VaccineDTO.builder()
                    .vaccineId(vaccine.getVaccineId().toString())
                    .vaccineName(vaccine.getVaccineName())
                    .vaccineType(vaccine.getVaccineTypeByVaccineTypeId().getVaccineTypeName())
                    .numberOfInjection(vaccine.getNumberOfInjection())
                    .origin(vaccine.getOrigin())
                    .status(vaccine.getStatus())
                    .build();
            vaccineDTOS.add(vaccineDTO);
        }
        return vaccineDTOS;
    }

    @Override
    public VaccineDTO getById(UUID id) {
        Vaccine vaccine = vaccineRepository.getById(id);
        VaccineDTO vaccineDTO = VaccineDTO.builder()
                .vaccineId(vaccine.getVaccineId().toString())
                .status(vaccine.getStatus())
                .vaccineName(vaccine.getVaccineName())
                .vaccineType(vaccineTypeRepository.getById(vaccine.getVaccineTypeByVaccineTypeId().getVaccineTypeId()).getVaccineTypeName())
                .numberOfInjection(vaccine.getNumberOfInjection())
                .indication(vaccine.getIndication())
                .contraindication(vaccine.getContraindication())
                .usage(vaccine.getUsage())
                .timeOfBeginningNextInjection(vaccine.getTimeBeginNextInjection())
                .timeOfEndingNextInjection(vaccine.getTimeEndNextInjection())
                .origin(vaccine.getOrigin())
                .build();

        return vaccineDTO;
    }

    @Override
    public Vaccine createNew(VaccineDTO vaccineDTO) {
        Vaccine vaccine = Vaccine.builder()
                .vaccineId(UUID.fromString(vaccineDTO.getVaccineId()))
                .status(vaccineDTO.getStatus())
                .vaccineName(vaccineDTO.getVaccineName())
                .vaccineTypeByVaccineTypeId(vaccineTypeRepository.findVaccineTypeByVaccineTypeName(vaccineDTO.getVaccineType()))
                .numberOfInjection(vaccineDTO.getNumberOfInjection())
                .usage(vaccineDTO.getUsage())
                .indication(vaccineDTO.getIndication())
                .contraindication(vaccineDTO.getContraindication())
                .timeBeginNextInjection(vaccineDTO.getTimeOfBeginningNextInjection())
                .origin(vaccineDTO.getOrigin())
                .build();
        if (vaccineDTO.getTimeOfEndingNextInjection().after(vaccineDTO.getTimeOfBeginningNextInjection()) ||
                vaccineDTO.getTimeOfEndingNextInjection() == vaccineDTO.getTimeOfBeginningNextInjection()) {
            vaccine.setTimeEndNextInjection(vaccineDTO.getTimeOfEndingNextInjection());
        }
        vaccineRepository.save(vaccine);
        return vaccine;
    }

    @Override
    public Vaccine update(VaccineDTO vaccineDTO) {
        Vaccine vaccine = Vaccine.builder()
                .vaccineId(UUID.fromString(vaccineDTO.getVaccineId()))
                .vaccineName(vaccineDTO.getVaccineName())
                .vaccineTypeByVaccineTypeId(vaccineTypeRepository.findVaccineTypeByVaccineTypeName(vaccineDTO.getVaccineType()))
                .numberOfInjection(vaccineDTO.getNumberOfInjection())
                .indication(vaccineDTO.getIndication())
                .contraindication(vaccineDTO.getContraindication())
                .usage(vaccineDTO.getUsage())
                .timeBeginNextInjection(vaccineDTO.getTimeOfBeginningNextInjection())
                .timeEndNextInjection(vaccineDTO.getTimeOfEndingNextInjection())
                .origin(vaccineDTO.getOrigin())
                .build();
        if (vaccineDTO.getStatus().equalsIgnoreCase("active")){
            vaccine.setStatus("Active");
        } else if (vaccineDTO.getStatus().equals("In-Active")) {
            vaccine.setStatus("In-Active");
        }
        vaccineRepository.save(vaccine);
        return vaccine;
    }

    @Override
    public Optional<Vaccine> findById(UUID id) {
        return vaccineRepository.findById(id);
    }

    @Override
    public Vaccine updateStatus(UUID id) {
        Vaccine vaccine = vaccineRepository.getById(id);
        vaccine.setStatus("In-Active");
        return vaccineRepository.save(vaccine);
    }


    @Override
    public List<VaccineReportDTO> getAllVaccineByFilter(String beginDate, String endDate,
                                                        String vaccineType,
                                                        String origin) {
        List<VaccineReportDTO> vaccineReportDTOS = new ArrayList<>();

        List<Vaccine> vaccines = vaccineRepository.getAllVaccineByFilter(beginDate, endDate,
                vaccineType, origin);

        for (Vaccine vaccine : vaccines) {
            VaccineReportDTO vaccineReportDTO = VaccineReportDTO.builder()
                    .vaccineId(vaccine.getVaccineId().toString())
                    .vaccineName(vaccine.getVaccineName())
                    .vaccineType(vaccine.getVaccineTypeByVaccineTypeId().getVaccineTypeName())
                    .numOfInject(vaccine.getNumberOfInjection())
                    .beginNextInjectDate(String.valueOf(vaccine.getTimeBeginNextInjection()))
                    .endNextInjectDate(String.valueOf(vaccine.getTimeEndNextInjection()))
                    .origin(vaccine.getOrigin())
                    .build();
            vaccineReportDTOS.add(vaccineReportDTO);
        }
        return vaccineReportDTOS;
    }

    @Override
    public List<Integer> getNumberVaccineByMonthAndYear(Integer year) {
        return vaccineRepository.getNumberVaccineByMonthAndYear(year);
    }




}
