package fa.training.services.impl;

import fa.training.models.dto.InjectionScheduleWithStatusDTO;
import fa.training.models.entity.InjectionSchedule;
import fa.training.repositories.InjectionScheduleRepository;
import fa.training.repositories.VaccineRepository;
import fa.training.services.InjectionService;
import java.sql.Date;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InjectionScheduleImpl implements InjectionService {

  @Autowired
  InjectionScheduleRepository injectionScheduleRepository;

  @Autowired
  VaccineRepository vaccineRepository;

  @Override
  public List<InjectionSchedule> getAll() {
    return injectionScheduleRepository.findAll();
  }

  @Override
  public List<InjectionScheduleWithStatusDTO> getAllSchedule() {
    List<InjectionScheduleWithStatusDTO> scheduleDTOS = new ArrayList<>();



    List<InjectionSchedule> schedules = injectionScheduleRepository.findAll();

    for (InjectionSchedule schedule : schedules) {
      InjectionScheduleWithStatusDTO scheduleDTO = InjectionScheduleWithStatusDTO.builder()
          .injectionScheduleId(schedule.getInjectionScheduleId().toString())
          .vaccine(schedule.getVaccineByVaccineId().getVaccineName())
          .startDate(schedule.getStartDate())
          .endDate(schedule.getEndDate())
          .status(schedule.getStatus())
          .place(schedule.getPlace())
          .note(schedule.getNote())
          .build();

      scheduleDTOS.add(scheduleDTO);
    }

    return scheduleDTOS;
  }

  @Override
  public InjectionSchedule save(InjectionSchedule injectionSchedule) {
    return injectionScheduleRepository.save(injectionSchedule);
  }

  @Override
  public InjectionScheduleWithStatusDTO createNew(InjectionScheduleWithStatusDTO withStatusDTO) {
    Date currentDate = new Date(System.currentTimeMillis());
    InjectionSchedule schedule = new InjectionSchedule();
    schedule.setVaccineByVaccineId(vaccineRepository.findByVaccineName(withStatusDTO.getVaccine()));
    schedule.setStartDate(withStatusDTO.getStartDate());
    if (withStatusDTO.getEndDate().after(withStatusDTO.getStartDate()) || withStatusDTO.getEndDate() == withStatusDTO.getStartDate()) {
      schedule.setEndDate(withStatusDTO.getEndDate());
    }
    if (currentDate.before(withStatusDTO.getStartDate())){
      schedule.setStatus("Not yet");
    } else if (currentDate.after(withStatusDTO.getEndDate()) || currentDate == (withStatusDTO.getEndDate())){
      schedule.setStatus("Over");
    } else if (currentDate.after(withStatusDTO.getEndDate()) && currentDate.before(withStatusDTO.getStartDate())) {
      schedule.setStatus("Open");
    }
    schedule.setPlace(withStatusDTO.getPlace());
    schedule.setNote(withStatusDTO.getNote());
    injectionScheduleRepository.save(schedule);

    return withStatusDTO;
  }

  @Override
  public InjectionScheduleWithStatusDTO getById(UUID id) {
    InjectionSchedule injectionSchedule = injectionScheduleRepository.getById(id);

    return InjectionScheduleWithStatusDTO.builder()
        .injectionScheduleId((injectionSchedule.getInjectionScheduleId()).toString())
        .vaccine(injectionSchedule.getVaccineByVaccineId().getVaccineName())
        .startDate(injectionSchedule.getStartDate())
        .endDate(injectionSchedule.getEndDate())
        .status(injectionSchedule.getStatus())
        .place(injectionSchedule.getPlace())
        .note(injectionSchedule.getNote())
        .build();
  }

  @Override
  public InjectionSchedule update(InjectionScheduleWithStatusDTO withStatusDTO) {
    InjectionSchedule schedule = injectionScheduleRepository.getById(UUID.fromString(withStatusDTO.getInjectionScheduleId()));
    schedule.setVaccineByVaccineId(vaccineRepository.findByVaccineName(withStatusDTO.getVaccine()));
    schedule.setStartDate(withStatusDTO.getStartDate());
    schedule.setEndDate(withStatusDTO.getEndDate());
    schedule.setStatus(withStatusDTO.getStatus());
    schedule.setPlace(withStatusDTO.getPlace());
    schedule.setNote(withStatusDTO.getNote());

    injectionScheduleRepository.save(schedule);

    return schedule;
  }

  @Override
  public Set<String> getAllStatus() {
    Set<String> statuses = new HashSet<>();

    List<InjectionSchedule> schedules = injectionScheduleRepository.findAll();

    for(InjectionSchedule schedule : schedules){
      statuses.add(schedule.getStatus());
    }

    return statuses;
  }


}
