package fa.training.services.impl;

import fa.training.models.dto.CustomerDTO;
import fa.training.models.dto.ListCustomerReportDTO;
import fa.training.models.entity.Customer;
import fa.training.models.entity.InjectionResult;
import fa.training.repositories.CustomerRepository;
import fa.training.repositories.InjectionResultRepository;
import fa.training.services.CustomerService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {

  @Autowired
  CustomerRepository customerRepository;

  @Autowired
  InjectionResultRepository injectionResultRepository;

  @Override
  public List<ListCustomerReportDTO> findAllCustomer() {
    List<ListCustomerReportDTO> reportDTOList = new ArrayList<>();

    List<Customer> customerList = customerRepository.findAll();

    List<InjectionResult> injectionResults = injectionResultRepository.findAll();
    int size = injectionResults.size();

    int index = 0;
    for (Customer customer : customerList) {
      if (index <= size) {
        ListCustomerReportDTO listCustomerReportDTO = ListCustomerReportDTO.builder()
            .customerId(customer.getCustomerId().toString())
            .fullName(customer.getFullName())
            .dateOfBirth(String.valueOf(customer.getDateOfBirth()))
            .address(customer.getAddress())
            .identityCard(customer.getIdentityCard())
            .numberOfInjection(String.valueOf(
                injectionResultRepository.findInjectByCustomerId(
                    customer.getCustomerId().toString())))
            .build();
        reportDTOList.add(listCustomerReportDTO);
      }
      index++;
    }
    return reportDTOList;
  }

  @Override
  public List<ListCustomerReportDTO> findCustomerByFilter(String fromDate, String toDate,
      String fullName, String address) {
    List<Customer> customerListByFilter = customerRepository.findCustomerByDOBAndFullNameAndAddress(
        fromDate, toDate, fullName, address);
    List<ListCustomerReportDTO> reportDTOList = new ArrayList<>();
    int size = injectionResultRepository.findAll().size();

    int index = 0;
    for (Customer customer : customerListByFilter) {
      if (index <= size) {
        ListCustomerReportDTO listCustomerReportDTO = ListCustomerReportDTO.builder()
            .customerId(customer.getCustomerId().toString())
            .fullName(customer.getFullName())
            .dateOfBirth(String.valueOf(customer.getDateOfBirth()))
            .address(customer.getAddress())
            .identityCard(customer.getIdentityCard())
            .numberOfInjection(String.valueOf(
                injectionResultRepository.findInjectByCustomerId(
                    customer.getCustomerId().toString())))
            .build();
        reportDTOList.add(listCustomerReportDTO);
      }
      index++;
    }
    return reportDTOList;
  }

  @Override
  public List<Integer> getNumberCustomerInjectionByMonthAndYear(Integer year) {
    return customerRepository.getNumberCustomerInjectionByMonthAndYear(year);
  }

  @Override
  public Customer saveCustomer(CustomerDTO customerDTO) {
    Customer customer = Customer.builder()
            .fullName(customerDTO.getFullName())
            .dateOfBirth(customerDTO.getDateOfBirth())
            .identityCard(customerDTO.getIdentityCard())
            .address(customerDTO.getAddress())
            .username(customerDTO.getUsername())
            .password(customerDTO.getPassword())
            .email(customerDTO.getEmail())
            .phone(customerDTO.getPhone())
            .build();
    if (customerDTO.getGender().equals("Male")){
      customer.setGender(1);
    } else if (customerDTO.getGender().equals("Female")) {
      customer.setGender(2);
    }
    customerRepository.save(customer);
    return customer;
  }

  @Override
  public List<CustomerDTO> getAll() {
    List<CustomerDTO> customerDTOs = new ArrayList<>();
    List<Customer> customers = customerRepository.findAll();
    for (Customer customer : customers) {
      CustomerDTO customerDTO = CustomerDTO.builder()
              .customerId(customer.getCustomerId())
              .fullName(customer.getFullName())
              .dateOfBirth(customer.getDateOfBirth())
              .address((customer.getAddress()))
              .identityCard(customer.getIdentityCard())
              .phone(customer.getPhone())
              .build();
      if (customer.getGender() == 1){
        customerDTO.setGender("Male");
      } else if (customer.getGender() == 2){
        customerDTO.setGender("Female");
      }
      customerDTOs.add(customerDTO);
    }
    return customerDTOs;
  }

  @Override
  public void delete(UUID id) {
    customerRepository.deleteById(id);
  }

  @Override
  public CustomerDTO getById(UUID id) {
    Customer customer = customerRepository.getById(id);
    CustomerDTO customerDTO = CustomerDTO.builder()
            .customerId(customer.getCustomerId())
            .fullName(customer.getFullName())
            .dateOfBirth(customer.getDateOfBirth())
            .identityCard(customer.getIdentityCard())
            .address(customer.getAddress())
            .username(customer.getUsername())
            .password(customer.getPassword())
            .email(customer.getEmail())
            .phone(customer.getPhone())
            .build();
    if (customer.getGender() == 1){
      customerDTO.setGender("Male");
    } else if (customer.getGender() == 2) {
      customerDTO.setGender("Female");
    }
    return customerDTO;
  }

  @Override
  public Customer update(CustomerDTO customerDTO) {
    Customer customer = Customer.builder()
            .customerId(customerDTO.getCustomerId())
            .fullName(customerDTO.getFullName())
            .dateOfBirth(customerDTO.getDateOfBirth())
            .identityCard(customerDTO.getIdentityCard())
            .address(customerDTO.getAddress())
            .username(customerDTO.getUsername())
            .password(customerDTO.getPassword())
            .email(customerDTO.getEmail())
            .phone(customerDTO.getPhone())
            .build();
    if (customerDTO.getGender().equals("Male")){
      customer.setGender(1);
    } else if (customerDTO.getGender().equals("Female")) {
      customer.setGender(2);
    }
    customerRepository.save(customer);
    return customer;
  }
}
