package fa.training.services.impl;

import fa.training.models.dto.VaccineTypeDTO;
import fa.training.models.entity.VaccineType;
import fa.training.repositories.VaccineTypeRepository;
import fa.training.services.VaccineTypeService;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VaccineTypeServiceImpl implements VaccineTypeService {

  @Autowired
  final VaccineTypeRepository vaccineTypeRepository;

  public VaccineTypeServiceImpl(VaccineTypeRepository vaccineTypeRepository) {
    this.vaccineTypeRepository = vaccineTypeRepository;
  }


  @Override
  public Set<String> getAllVaccineTypeNames() {
    Set<String> vaccineTypeNames = new HashSet<>();
    List<VaccineType> vaccineTypes = vaccineTypeRepository.findAll();
    for (VaccineType vaccineType : vaccineTypes) {
      vaccineTypeNames.add(vaccineType.getVaccineTypeName());
    }
    return vaccineTypeNames;
  }

  @Override
  public List<String> getAllVaccineTypeName() {
    return vaccineTypeRepository.getAllVaccineTypeName();
  }

  @Override
  public List<VaccineTypeDTO> getAll() {
    List<VaccineTypeDTO> vaccineTypeDTOS = new ArrayList<>();
    List<VaccineType> vaccineTypes = vaccineTypeRepository.findAll();
    for (VaccineType vaccineType : vaccineTypes) {
      VaccineTypeDTO vaccineTypeDTO = VaccineTypeDTO.builder()
          .vaccineTypeId(vaccineType.getVaccineTypeId().toString())
          .code(vaccineType.getCode())
          .vaccineTypeName(vaccineType.getVaccineTypeName())
          .description(vaccineType.getDescription())
          .status(vaccineType.getStatus())
          .build();
      vaccineTypeDTOS.add(vaccineTypeDTO);
    }
    return vaccineTypeDTOS;
  }

  @Override
  public VaccineType updateById(String id) {
    VaccineType vaccineTypeById = vaccineTypeRepository.findById(UUID.fromString(id)).get();
    vaccineTypeById.setStatus(0);
    return vaccineTypeRepository.save(vaccineTypeById);
  }

  @Override
  public VaccineType save(VaccineTypeDTO vaccineTypeDTO) {
    VaccineType vaccineType = VaccineType.builder()
        .code(vaccineTypeDTO.getCode())
        .vaccineTypeName(vaccineTypeDTO.getVaccineTypeName())
        .status(vaccineTypeDTO.getStatus())
        .description(vaccineTypeDTO.getDescription())
        .image(vaccineTypeDTO.getImage())
        .build();
    return vaccineTypeRepository.save(vaccineType);
  }
}
