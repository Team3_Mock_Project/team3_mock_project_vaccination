package fa.training.services.impl;

import fa.training.models.dto.InjectionResultReportDTO;
import fa.training.models.dto.InjectionResultDTO;
import fa.training.models.dto.InjectionScheduleWithStatusDTO;
import fa.training.models.entity.Customer;
import fa.training.models.entity.InjectionResult;
import fa.training.models.entity.Vaccine;
import fa.training.repositories.CustomerRepository;
import fa.training.repositories.InjectionResultRepository;
import fa.training.repositories.VaccineRepository;
import fa.training.services.InjectionResultService;

import java.sql.Date;
import java.util.*;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InjectionResultServiceImpl implements InjectionResultService {

  @Autowired
  InjectionResultRepository injectionResultRepository;

  @Autowired
  VaccineRepository vaccineRepository;

  @Autowired
  CustomerRepository customerRepository;

  @Override
  public List<Integer> getAllYearOfInjectionResult() {
    return injectionResultRepository.getYearOfInjectionResult();
  }

  @Override
  public List<Integer> getNumberInjectionByMonthAndYear(Integer year) {
    return injectionResultRepository.getNumberInjectionByMonthAndYear(year);
  }

  @Override
  public List<InjectionResultReportDTO> getAllInjectionResultReport() {
    List<InjectionResultReportDTO> injectionResultReportDTOS = new ArrayList<>();

    List<InjectionResult> injectionResults = injectionResultRepository.findAll();
    for (InjectionResult injectionResult : injectionResults) {
      UUID vaccineId = injectionResult.getVaccineByVaccineId().getVaccineId();
      UUID customerId = injectionResult.getCustomerByCustomerId().getCustomerId();
      InjectionResultReportDTO injectionResultReportDTO = InjectionResultReportDTO.builder()
              .injectionResultId(injectionResult.getInjectionResultId().toString())
              .vaccineName(vaccineRepository.getById(vaccineId).getVaccineName())
              .prevention(injectionResult.getPrevention())
              .fullName(customerRepository.getById(customerId).getFullName())
              .injectionDate(String.valueOf(injectionResult.getInjectionDate()))
              .numberOfInjection(injectionResult.getNumberOfInjection())
              .build();
      injectionResultReportDTOS.add(injectionResultReportDTO);
    }
    return injectionResultReportDTOS;
  }

  @Override
  public List<InjectionResultReportDTO> findInjectResultByInjectionDateAndPreventionAndVaccineType(
          String fromInjectDate, String toInjectDate, String prevention, String vaccineType
  ) {
    List<InjectionResultReportDTO> injectionResultReportDTOS = new ArrayList<>();
    List<InjectionResult> injectionResults = injectionResultRepository.findInjectResultByInjectionDateAndPreventionAndVaccineType(
            fromInjectDate, toInjectDate, prevention, vaccineType);

    for (InjectionResult injectionResult : injectionResults) {
      UUID vaccineId = injectionResult.getVaccineByVaccineId().getVaccineId();
      UUID customerId = injectionResult.getCustomerByCustomerId().getCustomerId();
      InjectionResultReportDTO injectionResultReportDTO = InjectionResultReportDTO.builder()
              .injectionResultId(injectionResult.getInjectionResultId().toString())
              .vaccineName(vaccineRepository.getById(vaccineId).getVaccineName())
              .prevention(injectionResult.getPrevention())
              .fullName(customerRepository.getById(customerId).getFullName())
              .injectionDate(String.valueOf(injectionResult.getInjectionDate()))
              .numberOfInjection(injectionResult.getNumberOfInjection())
              .build();
      injectionResultReportDTOS.add(injectionResultReportDTO);

    }
    return injectionResultReportDTOS;
  }

  //  @Override
//  public InjectionResultDTO createNew(InjectionResultDTO injectionResultDTO) {
//    InjectionResult result = new InjectionResult();
//    InjectionResultDTO dto = new InjectionResultDTO();
//    Customer customer = customerRepository.findById(injectionResultDTO.getCustomer().getCustomerId())
//            .orElseThrow(() -> new IllegalArgumentException("Customer didn't exist!"));
//    InjectionResult saveInjectionResult = injectionResultRepository.save(result);
//    dto.setCustomer(result.getCustomerByCustomerId());
//    dto.setVaccineName(result.getVaccineByVaccineId().getVaccineName());
//    dto.setPrevention(result.getPrevention());
//    dto.setNumberOfInject(result.getNumberOfInjection());
//    dto.setDateOfInject(String.valueOf(result.getInjectionDate()));
//    dto.setNextInjectDate(String.valueOf(result.getNextInjectionDate()));
//
//
//
//
//    return null;
  @Override
  public InjectionResultDTO createNew(InjectionResultDTO injectionResultDTO) {
    InjectionResult result = new InjectionResult();
    Customer customer = customerRepository.findById(injectionResultDTO.getCustomer().getCustomerId())
            .orElseThrow(() -> new IllegalArgumentException("Khách hàng không tồn tại!"));
    result.setCustomerByCustomerId(customer);
    result.setVaccineByVaccineId(vaccineRepository.findById(UUID.fromString(injectionResultDTO.getVaccineName()))
            .orElseThrow(() -> new IllegalArgumentException("Vaccine không tồn tại!")));
    result.setPrevention(injectionResultDTO.getPrevention());
    result.setNumberOfInjection(injectionResultDTO.getNumberOfInject());
    InjectionResult savedInjectionResult = injectionResultRepository.save(result);
    return mapToDto(savedInjectionResult);
  }

  private InjectionResultDTO mapToDto(InjectionResult result) {
    InjectionResultDTO dto = new InjectionResultDTO();
    dto.setCustomer(result.getCustomerByCustomerId());
    dto.setVaccineName(result.getVaccineByVaccineId().getVaccineName());
    dto.setPrevention(result.getPrevention());
    dto.setNumberOfInject(result.getNumberOfInjection());
    dto.setDateOfInject(String.valueOf(result.getInjectionDate()));
    dto.setNextInjectDate(String.valueOf(result.getNextInjectionDate()));

    return dto;
  }

  @Override
  public InjectionResult update(InjectionResultDTO injectionResultDTO) {

    InjectionResult result = injectionResultRepository.getById(injectionResultDTO.getInjectionResultId());


    Customer customer = (Customer) customerRepository.findCustomerByDOBAndFullNameAndAddress(injectionResultDTO.getCustomer());
    if (customer == null) {
      throw new EntityNotFoundException("Customer not found with provided details.");
    }
    result.setCustomerByCustomerId(customer);
    Vaccine vaccine = vaccineRepository.findByVaccineName(injectionResultDTO.getVaccineName());
    if (vaccine == null) {
      throw new EntityNotFoundException("Vaccine not found with name: " + injectionResultDTO.getVaccineName());
    }
    result.setVaccineByVaccineId(vaccine);


    result.setPrevention(injectionResultDTO.getPrevention());
    result.setNumberOfInjection(injectionResultDTO.getNumberOfInject());
    result.setInjectionDate(Date.valueOf(injectionResultDTO.getDateOfInject()));
    result.setNextInjectionDate(Date.valueOf(injectionResultDTO.getNextInjectDate()));

    injectionResultRepository.save(result);

    return result;
  }

  @Override
  public void deleteInjectionResultById(UUID uuid) {
    Optional<InjectionResult> resultOptional = injectionResultRepository.findById(uuid);
    if (resultOptional.isPresent()) {
      injectionResultRepository.deleteById(uuid);
    } else {
      throw new EntityNotFoundException("InjectionResult not found for ID: " + uuid);
    }
  }

  @Override
  public Set<InjectionResultDTO> getAllInjectionResult() {
    Set<InjectionResultDTO> injectionResultDTOS = new HashSet<>();
    List<InjectionResult> injectionResults = injectionResultRepository.findAll();
    for (InjectionResult injectionResult : injectionResults) {
      InjectionResultDTO dto = new InjectionResultDTO();
      dto.setCustomer(injectionResult.getCustomerByCustomerId());
      dto.setVaccineName(injectionResult.getVaccineByVaccineId().getVaccineName());
      dto.setPrevention(injectionResult.getPrevention());
      dto.setDateOfInject(String.valueOf(injectionResult.getInjectionDate()));
      dto.setNextInjectDate(String.valueOf(injectionResult.getInjectionDate()));
      injectionResultDTOS.add(dto);
    }
    return injectionResultDTOS;


  }

  @Override
  public Set<String> getAllPreventions() {
    Set<String> allPreventions = new HashSet<>();
    List<InjectionResult> injectionResults = injectionResultRepository.findAll();
    for(InjectionResult injectionResult : injectionResults) {
      allPreventions.add(injectionResult.getPrevention());
    }
    return allPreventions;
  }

  @Override
  public Set<String> getAllPlaces() {
    Set<String> allPlaces = new HashSet<>();
    List<InjectionResult> injectionResults = injectionResultRepository.findAll();
    for(InjectionResult injectionResult : injectionResults) {
      allPlaces.add(injectionResult.getInjectionPlace());
    }
    return allPlaces;
  }

  @Override
  public InjectionResult save(InjectionResult injectionResult) {
    return injectionResultRepository.save(injectionResult);
  }

  @Override
  public InjectionResultDTO getById(UUID id) {
    InjectionResult injectionResult = injectionResultRepository.getById(id);
  return    InjectionResultDTO.builder()
            .customer(injectionResult.getCustomerByCustomerId())
            .vaccineName(injectionResult.getVaccineByVaccineId().getVaccineName())
            .prevention(injectionResult.getPrevention())
            .numberOfInject(injectionResult.getNumberOfInjection())
            .dateOfInject(String.valueOf(injectionResult.getInjectionDate()))
            .nextInjectDate(String.valueOf(injectionResult.getNextInjectionDate()))
            .build();
  }
}





