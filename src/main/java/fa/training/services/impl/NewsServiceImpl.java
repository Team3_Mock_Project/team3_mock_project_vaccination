package fa.training.services.impl;

import fa.training.models.dto.NewsDTO;
import fa.training.models.entity.News;
import fa.training.repositories.NewsRepository;
import fa.training.services.NewsService;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NewsServiceImpl implements NewsService {

  @Autowired
  NewsRepository newsRepository;

  @Override
  public List<NewsDTO> getAllNews() {
    List<NewsDTO> newsDTOList = new ArrayList<>();
    List<News> newsList = newsRepository.findAll();
    for (News news : newsList) {
      NewsDTO newsDTO = NewsDTO.builder()
          .newsId(news.getNewsId().toString())
          .title(news.getTitle())
          .content(news.getContent())
          .postDate(String.valueOf(news.getPostDate()))
          .build();
      newsDTOList.add(newsDTO);
    }
    return newsDTOList;
  }

  @Override
  public void deleteById(UUID id) {
    newsRepository.deleteById(id);
  }

  @Override
  public News save(NewsDTO newsDTO) {
    News news = News.builder()
        .title(newsDTO.getTitle())
        .preview(newsDTO.getPreview())
        .content(newsDTO.getContent())
        .postDate(Date.valueOf(LocalDate.now()))
        .build();
    return newsRepository.save(news);
  }

  @Override
  public NewsDTO findById(String id) {
    News news = newsRepository.getById(UUID.fromString(id));
    return NewsDTO.builder()
        .newsId(news.getNewsId().toString())
        .title(news.getTitle())
        .preview(news.getPreview())
        .content(news.getContent())
        .postDate(String.valueOf(news.getPostDate()))
        .build();
  }

  @Override
  public News updateById(NewsDTO newsDTO) {
    News news = News.builder()
        .newsId(UUID.fromString(newsDTO.getNewsId()))
        .title(newsDTO.getTitle())
        .preview(newsDTO.getPreview())
        .content(newsDTO.getContent())
        .postDate(Date.valueOf(LocalDate.now()))
        .build();
    return newsRepository.save(news);
  }
}
