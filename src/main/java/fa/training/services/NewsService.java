package fa.training.services;

import fa.training.models.dto.NewsDTO;
import fa.training.models.entity.News;
import java.util.List;
import java.util.UUID;


public interface NewsService {

  List<NewsDTO> getAllNews();

  void deleteById(UUID id);

  News save(NewsDTO newsDTO);

  NewsDTO findById(String id);

  News updateById(NewsDTO newsDTO);
}
