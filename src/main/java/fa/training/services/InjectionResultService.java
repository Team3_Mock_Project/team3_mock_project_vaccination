package fa.training.services;

import fa.training.models.dto.InjectionResultReportDTO;
import fa.training.models.dto.InjectionResultDTO;
import fa.training.models.entity.InjectionResult;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface InjectionResultService {
  List<Integer> getAllYearOfInjectionResult();

  List<Integer> getNumberInjectionByMonthAndYear(Integer year);

  List<InjectionResultReportDTO> getAllInjectionResultReport();

  List<InjectionResultReportDTO> findInjectResultByInjectionDateAndPreventionAndVaccineType(
      String fromInjectDate, String toInjectDate, String prevention, String vaccineType);

  InjectionResultDTO createNew(InjectionResultDTO injectionResultDTO);
  InjectionResult update(InjectionResultDTO injectionResultDTO);

  void deleteInjectionResultById(UUID uuid);
  Set<InjectionResultDTO> getAllInjectionResult();
  Set<String> getAllPreventions();
  Set<String> getAllPlaces();
  InjectionResult save(InjectionResult injectionResult);
  InjectionResultDTO getById(UUID id);
}
