package fa.training.services;

import fa.training.models.dto.CustomerDTO;
import fa.training.models.dto.ListCustomerReportDTO;
import fa.training.models.entity.Customer;

import java.util.List;
import java.util.UUID;

public interface CustomerService {
  List<ListCustomerReportDTO> findAllCustomer();

  List<ListCustomerReportDTO> findCustomerByFilter(String fromDate, String toDate, String fullName,
      String address);

  List<Integer> getNumberCustomerInjectionByMonthAndYear(Integer year);

    Customer saveCustomer(CustomerDTO customerDTO);

  List<CustomerDTO> getAll();

  void delete(UUID id);

  CustomerDTO getById(UUID id);

  Customer update(CustomerDTO customerDTO);
}
