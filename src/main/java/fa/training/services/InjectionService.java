package fa.training.services;
import fa.training.models.dto.InjectionScheduleWithStatusDTO;
import fa.training.models.entity.InjectionSchedule;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;
import java.util.Set;
import java.util.UUID;


public interface InjectionService {

    List<InjectionSchedule> getAll();

    List<InjectionScheduleWithStatusDTO> getAllSchedule();

    InjectionSchedule save(InjectionSchedule injectionSchedule);

    InjectionScheduleWithStatusDTO createNew(InjectionScheduleWithStatusDTO withStatusDTO) throws ParseException;

    InjectionScheduleWithStatusDTO getById(UUID id);

    InjectionSchedule update(InjectionScheduleWithStatusDTO withStatusDTO);

    Set<String> getAllStatus();

}
