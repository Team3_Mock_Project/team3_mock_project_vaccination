package fa.training.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum UserStatusConstants {
  ROLE_ADMIN,
  ROLE_EMPLOYEE,
  ;
}
