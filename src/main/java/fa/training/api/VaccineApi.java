package fa.training.api;

import fa.training.models.dto.VaccineDTO;
import fa.training.models.entity.Vaccine;
import fa.training.services.VaccineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class VaccineApi {

    @Autowired
    final VaccineService vaccineService;

    public VaccineApi(VaccineService vaccineService) {
        this.vaccineService = vaccineService;
    }

    @GetMapping("api/vaccine")
    public List<VaccineDTO> getAllVaccine(){
        return vaccineService.getAllVaccine();
    }

    @PostMapping("api/vaccine/new")
    public Vaccine createNewVaccine(VaccineDTO vaccineDTO){
        return vaccineService.createNew(vaccineDTO);
    }

    @GetMapping("api/update/vaccine/{id}")
    public VaccineDTO getById(@PathVariable("id") UUID id){
        return vaccineService.getById(id);
    }

    @PostMapping("api/update/status/{id}")
    public Vaccine updateVaccineStatus(@PathVariable("id") UUID id){
        return vaccineService.updateStatus(id);
    }


}
