package fa.training.api;

import fa.training.models.dto.CustomerDTO;
import fa.training.models.entity.Customer;
import fa.training.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/")
public class CustomerApi {

    @Autowired
    final CustomerService customerService;

    public CustomerApi(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/customer")
    public List<CustomerDTO> getAllCustomer(){
        return customerService.getAll();
    }

    @PostMapping("/create/customer")
    public Customer saveCustomer(@RequestBody CustomerDTO customerDTO) throws IOException {
        return customerService.saveCustomer(customerDTO);
    }

    @DeleteMapping("/delete/customer/{id}")
    public ResponseEntity<Void> deleteCustomer(@PathVariable UUID id){
        customerService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/update/customer/{id}")
    public CustomerDTO updateCustomerUI(@PathVariable UUID id){
        return customerService.getById(id);
    }

    @PutMapping("/update/customer")
    public Customer updateCustomer(@RequestBody CustomerDTO customerDTO){
        return customerService.update(customerDTO);
    }
}
