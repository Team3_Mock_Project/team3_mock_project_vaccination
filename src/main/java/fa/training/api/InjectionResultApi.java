package fa.training.api;

import fa.training.models.dto.InjectionResultDTO;
import fa.training.models.entity.InjectionResult;
import fa.training.services.InjectionResultService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;
import java.util.UUID;

@RestController
public class InjectionResultApi {
    @Autowired
    private InjectionResultService injectionResultService;
    public InjectionResultApi(InjectionResultService injectionResultService) {
        this.injectionResultService =  injectionResultService;
    }
    @GetMapping("api/injection-result")
    public Set<InjectionResultDTO> injectionResultDTOSet() {
        return injectionResultService.getAllInjectionResult();
    }
    @PutMapping("api/create/injection-result")
    public InjectionResult createInjectionResult(@ModelAttribute("injectionResultDTO") InjectionResultDTO injectionResultDTO) {
        InjectionResult injectionResult = new InjectionResult();
        BeanUtils.copyProperties(injectionResultDTO, injectionResult);
        return injectionResultService.save(injectionResult);
    }
    @GetMapping("api/update/injection-result")
    public InjectionResultDTO updateInjectionResult(@PathVariable("id") UUID id) {
        return injectionResultService.getById(id);
    }
    @DeleteMapping("api/delete/injection-result")
    public void deleteInjectionResult(@PathVariable("id") UUID id) {
        injectionResultService.deleteInjectionResultById(id);
    }

}
