package fa.training.api;

import fa.training.models.dto.VaccineTypeDTO;
import fa.training.models.entity.VaccineType;
import fa.training.services.VaccineTypeService;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/vaccineType")
public class VaccineTypeApi {

  @Autowired
  VaccineTypeService vaccineTypeService;

  @GetMapping("/list")
  public List<VaccineTypeDTO> getDataUI() {
    return vaccineTypeService.getAll();
  }

  @PostMapping("/update/{id}")
  public VaccineType makeInactiveVaccineType(@PathVariable("id") String id) {
    return vaccineTypeService.updateById(id);
  }

  @PostMapping("/create")
  public VaccineType createVaccineType(/*@RequestParam("image") MultipartFile multipartFile*/
      @RequestBody VaccineTypeDTO vaccineTypeDTO)
      throws IOException {
/*    String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
    String uploadDir = new File("target/classes/static/images").getAbsolutePath();
    FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);*/
    return vaccineTypeService.save(vaccineTypeDTO);
  }
}
