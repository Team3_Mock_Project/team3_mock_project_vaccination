package fa.training.api;

import fa.training.models.dto.InjectionResultReportDTO;
import fa.training.services.InjectionResultService;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ReportInjectionResultApi {

  @Autowired
  InjectionResultService injectionResultService;

  @GetMapping("/injectionResult/report")
  public List<InjectionResultReportDTO> getReportDataUI() {
    return injectionResultService.getAllInjectionResultReport();
  }

  @GetMapping("/injectionResult/filterReport")
  public List<InjectionResultReportDTO> listFilterInjectionResultReport(
      @RequestParam("fromInjectDate") String fromInjectDate,
      @RequestParam("toInjectDate") String toInjectDate,
      @RequestParam("prevention") String prevention,
      @RequestParam("vaccineType") String vaccineType
  ) {

    return injectionResultService.findInjectResultByInjectionDateAndPreventionAndVaccineType(
        fromInjectDate, toInjectDate, prevention, vaccineType);
  }

  @GetMapping("/injectionResult/statistic")
  public List<Integer> listNumberInjection(
      @RequestParam(value = "yearSelected", required = false) String year) {
    if (year == null) {
      return Collections.emptyList();
    } else {
      try {
        int yearValue = Integer.parseInt(year);
        return injectionResultService.getNumberInjectionByMonthAndYear(yearValue);
      } catch (NumberFormatException e) {

        return Collections.emptyList();
      }
    }
  }

}
