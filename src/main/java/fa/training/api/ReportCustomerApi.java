package fa.training.api;

import fa.training.models.dto.ListCustomerReportDTO;
import fa.training.services.CustomerService;
import fa.training.services.InjectionResultService;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ReportCustomerApi {

  @Autowired
  CustomerService customerService;

  @Autowired
  InjectionResultService injectionResultService;

  @GetMapping("/customer/report")
  public List<ListCustomerReportDTO> getReportDataUI() {
    return customerService.findAllCustomer();
  }

  @GetMapping("/customer/statistic")
  public List<Integer> listNumberInjection(
      @RequestParam(value = "yearSelected", required = false) String year) {
    if (year == null) {
      return Collections.emptyList();
    } else {
      try {
        int yearValue = Integer.parseInt(year);
        return customerService.getNumberCustomerInjectionByMonthAndYear(yearValue);
      } catch (NumberFormatException e) {

        return Collections.emptyList();
      }
    }
  }

  @GetMapping("/customer/filterReport")
  public List<ListCustomerReportDTO> listFilterCustomerReport(
      @RequestParam("fromDate") String fromDate,
      @RequestParam("toDate") String toDate,
      @RequestParam("fullName") String fullName,
      @RequestParam("address") String address
  ) {

    return customerService.findCustomerByFilter(fromDate, toDate, fullName, address);
  }
}
