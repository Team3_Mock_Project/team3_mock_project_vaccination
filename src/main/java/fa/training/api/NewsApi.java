package fa.training.api;

import fa.training.models.dto.NewsDTO;
import fa.training.models.entity.News;
import fa.training.services.NewsService;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NewsApi {

  @Autowired
  NewsService newsService;

  @GetMapping("/api/news/list")
  public List<NewsDTO> getDataUI() {
    return newsService.getAllNews();
  }

  @GetMapping("/api/news/delete/{id}")
  public ResponseEntity<Void> deleteNews(@PathVariable String id) {
    newsService.deleteById(UUID.fromString(id));
    return ResponseEntity.noContent().build();
  }

  @PostMapping("/api/news/create")
  public News createNews(@RequestBody NewsDTO newsDTO) {
    newsDTO.setPostDate(String.valueOf(LocalDate.now()));
    return newsService.save(newsDTO);
  }


  @GetMapping("/api/news/update/{id}")
  public NewsDTO getDataById(@PathVariable("id") String id) {
    return newsService.findById(id);
  }

  @PostMapping("/api/news/update")
  public News updateNews(@RequestBody NewsDTO newsDTO) {
    newsDTO.setPostDate(String.valueOf(LocalDate.now()));
    return newsService.updateById(newsDTO);
  }
}
