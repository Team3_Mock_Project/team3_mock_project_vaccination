package fa.training.api;

import fa.training.models.dto.VaccineReportDTO;
import fa.training.services.VaccineService;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ReportVaccineApi {

  @Autowired
  VaccineService vaccineService;

  @GetMapping("/vaccine/report")
  public List<VaccineReportDTO> getReportDataUI() {
    return vaccineService.getAllVaccineReport();
  }

  @GetMapping("/vaccine/filterReport")
  public List<VaccineReportDTO> listFilterVaccineReport(
      @RequestParam("beginDate") String beginDate,
      @RequestParam("endDate") String endDate,
      @RequestParam("vaccineType") String vaccineType,
      @RequestParam("origin") String origin
  ) {

    return vaccineService.getAllVaccineByFilter(beginDate, endDate, vaccineType, origin);
  }

  @GetMapping("/vaccine/statistic")
  public List<Integer> listNumberInjection(
      @RequestParam(value = "yearSelected", required = false) String year) {
    if (year == null) {
      return Collections.emptyList();
    } else {
      try {
        int yearValue = Integer.parseInt(year);
        return vaccineService.getNumberVaccineByMonthAndYear(yearValue);
      } catch (NumberFormatException e) {

        return Collections.emptyList();
      }
    }
  }

}
