package fa.training.api;

import fa.training.models.dto.InjectionScheduleWithStatusDTO;
import fa.training.models.entity.InjectionSchedule;
import fa.training.services.InjectionService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class InjectionScheduleApi {

    @Autowired
    final InjectionService injectionService;

    public InjectionScheduleApi(InjectionService injectionService) {
        this.injectionService = injectionService;
    }

    @GetMapping("api/injection/schedule")
    public List<InjectionScheduleWithStatusDTO> listInjectionSchedule() {
        return injectionService.getAllSchedule();
    }

    @PutMapping("api/create/injection/schedule")
    public InjectionSchedule createInjectionSchedule(@ModelAttribute("injectionScheduleWithStatusDTO") InjectionScheduleWithStatusDTO injectionScheduleDTO) {

        InjectionSchedule injectionSchedule = new InjectionSchedule();

        BeanUtils.copyProperties(injectionScheduleDTO, injectionSchedule);

        return injectionService.save(injectionSchedule);
    }

    @GetMapping("/api/update/injection/schedule/{id}")
    public InjectionScheduleWithStatusDTO updateInjectionSchedule(@PathVariable("id") UUID id) {
        return injectionService.getById(id);
    }
}
