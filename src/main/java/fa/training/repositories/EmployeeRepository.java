package fa.training.repositories;

import fa.training.models.entity.Employee;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, UUID> {

  Employee findAdminByUsernameAndPassword(String username, String password);

  Optional<Employee> findByUsername(String username);
}
