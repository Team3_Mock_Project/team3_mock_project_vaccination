package fa.training.repositories;

import fa.training.models.entity.NewsType;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface NewsTypeRepository extends JpaRepository<NewsType, UUID> {

}
