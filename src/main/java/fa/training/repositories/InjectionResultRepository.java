package fa.training.repositories;

import fa.training.models.entity.InjectionResult;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface InjectionResultRepository extends JpaRepository<InjectionResult, UUID> {

  @Query(value = "SELECT COUNT(NUMBER_OF_INJECTION) FROM VACCINE.INJECTION_RESULT WHERE CUSTOMER_ID = :customerId"
      , nativeQuery = true)
  int findInjectByCustomerId(@Param("customerId") String customerId);


  @Query(value = "SELECT DISTINCT YEAR(INJECTION_DATE)\n"
      + "FROM VACCINE.INJECTION_RESULT", nativeQuery = true)
  List<Integer> getYearOfInjectionResult();

  String queryNumberInjectionByMonthAndYear = """
      WITH AllMonths AS (
          SELECT 1 AS MONTH_NUMBER
          UNION ALL
          SELECT MONTH_NUMBER + 1
          FROM AllMonths
          WHERE MONTH_NUMBER < 12
      )
      SELECT COALESCE(SUM(v.NUMBER_OF_INJECTION), 0) AS NUMBER_OF_INJECTION
      FROM AllMonths m
      LEFT JOIN VACCINE.INJECTION_RESULT ir
      ON DATEPART(MONTH, ir.INJECTION_DATE) = m.MONTH_NUMBER AND DATEPART(YEAR, ir.INJECTION_DATE) = :YEAR
      LEFT JOIN VACCINE.VACCINE v ON ir.VACCINE_ID = v.VACCINE_ID
      GROUP BY m.MONTH_NUMBER
      ORDER BY m.MONTH_NUMBER;
      """;

  @Query(value = queryNumberInjectionByMonthAndYear, nativeQuery = true)
  List<Integer> getNumberInjectionByMonthAndYear(@Param("YEAR") Integer year);

  String getAllInjectionResultByFilter = """
      SELECT ir.*
      FROM VACCINE.INJECTION_RESULT ir
      INNER JOIN VACCINE.VACCINE v ON ir.VACCINE_ID = v.VACCINE_ID
      INNER JOIN VACCINE.VACCINE_TYPE vt ON v.VACCINE_TYPE_ID = vt.VACCINE_TYPE_ID
      WHERE ir.INJECTION_DATE  BETWEEN :fromInjectDate AND :toInjectDate
      AND ir.PREVENTION LIKE %:prevention%
      AND vt.VACCINE_TYPE_NAME LIKE %:vaccineType%
      """;

  @Query(value = getAllInjectionResultByFilter, nativeQuery = true)
  List<InjectionResult> findInjectResultByInjectionDateAndPreventionAndVaccineType(
      @Param("fromInjectDate") String fromInjectDate,
      @Param("toInjectDate") String toInjectDate,
      @Param("prevention") String prevention,
      @Param("vaccineType") String vaccineType);
}
