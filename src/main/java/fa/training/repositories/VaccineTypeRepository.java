package fa.training.repositories;

import fa.training.models.entity.VaccineType;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface VaccineTypeRepository extends JpaRepository<VaccineType, UUID> {

  String getAllVaccineTypeName = """
      SELECT DISTINCT vt.VACCINE_TYPE_NAME
      FROM VACCINE.VACCINE_TYPE vt
      """;

  @Query(value = getAllVaccineTypeName, nativeQuery = true)
  List<String> getAllVaccineTypeName();

  VaccineType findVaccineTypeByVaccineTypeName(String name);
}
