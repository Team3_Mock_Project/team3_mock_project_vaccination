package fa.training.repositories;

import fa.training.models.entity.InjectionSchedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface InjectionScheduleRepository extends JpaRepository<InjectionSchedule, UUID> {



}
