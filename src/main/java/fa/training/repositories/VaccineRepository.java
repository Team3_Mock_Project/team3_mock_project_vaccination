package fa.training.repositories;

import fa.training.models.entity.Vaccine;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface VaccineRepository extends JpaRepository<Vaccine, UUID> {

  Vaccine findByVaccineName(String vaccineName);

  String getAllVaccineByFilter = """
      SELECT v.*
      FROM VACCINE.VACCINE v
      INNER JOIN VACCINE.INJECTION_RESULT ir ON v.VACCINE_ID = ir.VACCINE_ID
      INNER JOIN VACCINE.VACCINE_TYPE vt ON v.VACCINE_TYPE_ID  = vt.VACCINE_TYPE_ID
      WHERE v.TIME_BEGIN_NEXT_INJECTION >= :beginDate
      AND v.TIME_END_NEXT_INJECTION <= :endDate
      AND vt.VACCINE_TYPE_NAME LIKE %:vaccineType%
      AND v.ORIGIN LIKE %:origin%
      """;

  @Query(value = getAllVaccineByFilter, nativeQuery = true)
  List<Vaccine> getAllVaccineByFilter(
      @Param("beginDate") String beginDate,
      @Param("endDate") String endDate,
      @Param("vaccineType") String vaccineType,
      @Param("origin") String origin);

  String queryNumberVaccineByMonthAndYear = """
      WITH AllMonths AS (
          SELECT 1 AS MONTH_NUMBER
          UNION ALL
          SELECT MONTH_NUMBER + 1
          FROM AllMonths
          WHERE MONTH_NUMBER < 12
      )
      SELECT COALESCE(COUNT(v.VACCINE_ID), 0) AS NUMBER_OF_INJECTION
      FROM AllMonths m
      LEFT JOIN VACCINE.INJECTION_RESULT ir ON DATEPART(MONTH, ir.INJECTION_DATE) = m.MONTH_NUMBER
      AND DATEPART(YEAR, ir.INJECTION_DATE) = :YEAR
      LEFT JOIN VACCINE.VACCINE v ON ir.VACCINE_ID = v.VACCINE_ID
      GROUP BY m.MONTH_NUMBER
      ORDER BY m.MONTH_NUMBER;
      """;

  @Query(value = queryNumberVaccineByMonthAndYear, nativeQuery = true)
  List<Integer> getNumberVaccineByMonthAndYear(@Param("YEAR") Integer year);
}
