package fa.training.models.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.sql.Date;
import java.util.List;
import java.util.UUID;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "VACCINE", schema = "VACCINE", catalog = "VACCINE_MANAGEMENT")
public class Vaccine {

  @Id
  @Column(name = "VACCINE_ID", nullable = false, length = 36)
  private UUID vaccineId;

  @Column(name = "STATUS")
  private String status;

  @Column(name = "CONTRAINDICATION", length = 200)
  private String contraindication;

  @Column(name = "INDICATION", length = 200)
  private String indication;

  @Column(name = "NUMBER_OF_INJECTION")
  private Integer numberOfInjection;

  @Column(name = "ORIGIN", length = 50)
  private String origin;

  @Column(name = "TIME_BEGIN_NEXT_INJECTION")
  private Date timeBeginNextInjection;

  @Column(name = "TIME_END_NEXT_INJECTION")
  private Date timeEndNextInjection;

  @Column(name = "USAGE", length = 200)
  private String usage;

  @Column(name = "VACCINE_NAME", length = 100)
  private String vaccineName;

  @OneToMany(mappedBy = "vaccineByVaccineId", cascade = CascadeType.ALL)
  private List<InjectionResult> injectionResultsByVaccineId;

  @OneToMany(mappedBy = "vaccineByVaccineId", cascade = CascadeType.ALL)
  private List<InjectionSchedule> injectionSchedulesByVaccineId;

  @ManyToOne
  @JoinColumn(name = "VACCINE_TYPE_ID", referencedColumnName = "VACCINE_TYPE_ID")
  private VaccineType vaccineTypeByVaccineTypeId;
}
