package fa.training.models.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.sql.Date;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "INJECTION_SCHEDULE", schema = "VACCINE", catalog = "VACCINE_MANAGEMENT")
public class InjectionSchedule {

  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  @Column(name = "INJECTION_SCHEDULE_ID", nullable = false, length = 36)
  private UUID injectionScheduleId;

  @Column(name = "DESCRIPTION", length = 1000)
  private String description;

  @Column(name = "END_DATE")
  private Date endDate;

  @Column(name = "PLACE")
  private String place;

  @Column(name = "START_DATE")
  private Date startDate;

  @Column(name = "STATUS")
  private String status;

  @Column(name = "NOTE")
  private String note;

  @ManyToOne
  @JsonIgnore
  @JoinColumn(name = "VACCINE_ID", referencedColumnName = "VACCINE_ID")
  private Vaccine vaccineByVaccineId;

}
