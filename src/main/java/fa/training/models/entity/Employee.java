package fa.training.models.entity;

import jakarta.persistence.*;

import java.sql.Date;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "EMPLOYEE", schema = "VACCINE", catalog = "VACCINE_MANAGEMENT")
public class Employee {

  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  @Column(name = "EMPLOYEE_ID", nullable = false, length = 36)
  private UUID employeeId;

  @Column(name = "ADDRESS")
  private String address;

  @Column(name = "DATE_OF_BIRTH")
  private Date dateOfBirth;

  @Column(name = "EMAIL", length = 100)
  private String email;

  @Column(name = "EMPLOYEE_NAME", length = 100)
  private String employeeName;

  @Column(name = "GENDER")
  private Integer gender;

  @Column(name = "IMAGE")
  private String image;

  @Column(name = "PASSWORD")
  private String password;

  @Column(name = "PHONE", length = 20)
  private String phone;

  @Column(name = "POSITION", length = 100)
  private String position;

  @Column(name = "USERNAME")
  private String username;

  @Column(name = "WORKING_PLACE")
  private String workingPlace;

}
