package fa.training.models.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.sql.Date;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "NEWS", schema = "VACCINE", catalog = "VACCINE_MANAGEMENT")
public class News {

  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  @Column(name = "NEWS_ID", nullable = false, length = 36)
  private UUID newsId;


  @Column(name = "CONTENT", length = 4000)
  private String content;


  @Column(name = "PREVIEW", length = 1000)
  private String preview;


  @Column(name = "TITLE", length = 300)
  private String title;

  @Column(name = "POSTDATE")
  private Date postDate;

  @ManyToOne
  @JoinColumn(name = "NEWS_TYPE_ID", referencedColumnName = "NEWS_TYPE_ID")
  private NewsType newsTypeByNewsTypeId;
}
