package fa.training.models.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "NEWS_TYPE", schema = "VACCINE", catalog = "VACCINE_MANAGEMENT")
public class NewsType {

  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  @Column(name = "NEWS_TYPE_ID", nullable = false, length = 36)
  private UUID newsTypeId;


  @Column(name = "DESCRIPTION", length = 10)
  private String description;


  @Column(name = "NEWS_TYPE_NAME", length = 50)
  private String newsTypeName;

  @OneToMany(mappedBy = "newsTypeByNewsTypeId")
  private List<News> newsByNewsTypeId;
}
