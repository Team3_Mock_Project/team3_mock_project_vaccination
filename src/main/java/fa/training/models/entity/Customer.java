package fa.training.models.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.sql.Date;
import java.util.List;
import java.util.UUID;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@Table(name = "CUSTOMER", schema = "VACCINE", catalog = "VACCINE_MANAGEMENT")
public class Customer {

  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  @Column(name = "CUSTOMER_ID", nullable = false, length = 36)
  private UUID customerId;

  @Column(name = "ADDRESS")
  private String address;

  @Column(name = "DATE_OF_BIRTH")
  private Date dateOfBirth;

  @Column(name = "EMAIL", length = 100)
  private String email;

  @Column(name = "FULL_NAME", length = 100)
  private String fullName;

  @Column(name = "GENDER")
  private Integer gender;

  @Column(name = "IDENTITY_CARD", length = 12)
  private String identityCard;

  @Column(name = "PASSWORD")
  private String password;

  @Column(name = "PHONE", length = 20)
  private String phone;

  @Column(name = "USERNAME")
  private String username;

  @JsonIgnore
  @OneToMany(mappedBy = "customerByCustomerId", cascade = CascadeType.ALL)
  private List<InjectionResult> injectionResultsByCustomerId;
}
