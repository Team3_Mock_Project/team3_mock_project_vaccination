package fa.training.models.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "VACCINE_TYPE", schema = "VACCINE", catalog = "VACCINE_MANAGEMENT")
public class VaccineType implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  @Column(name = "VACCINE_TYPE_ID", nullable = false, length = 36)
  private UUID vaccineTypeId;


  @Column(name = "DESCRIPTION", length = 200)
  private String description;


  @Column(name = "VACCINE_TYPE_NAME", length = 50)
  private String vaccineTypeName;

  @Column(name = "VACCINE_TYPE_CODE", length = 10)
  private String code;

  @Column(name = "VACCINE_TYPE_STATUS")
  private Integer status;

  @Column(name = "VACCINE_TYPE_IMAGE")
  private String image;

  @OneToMany(mappedBy = "vaccineTypeByVaccineTypeId", cascade = CascadeType.ALL)
  private List<Vaccine> vaccinesByVaccineTypeId;
}
