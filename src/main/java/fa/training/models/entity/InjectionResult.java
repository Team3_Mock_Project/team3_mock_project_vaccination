package fa.training.models.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.sql.Date;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "INJECTION_RESULT", schema = "VACCINE", catalog = "VACCINE_MANAGEMENT")
public class InjectionResult {

  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  @Column(name = "INJECTION_RESULT_ID", nullable = false, length = 36)
  private UUID injectionResultId;

  @Column(name = "INJECTION_DATE")
  private Date injectionDate;

  @Column(name = "INJECTION_PLACE")
  private String injectionPlace;

  @Column(name = "NEXT_INJECTION_DATE")
  private Date nextInjectionDate;

  @Column(name = "NUMBER_OF_INJECTION")
  private Integer numberOfInjection;

  @Column(name = "PREVENTION", length = 100)
  private String prevention;

  @ManyToOne
  @JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
  private Customer customerByCustomerId;

  @ManyToOne
  @JoinColumn(name = "VACCINE_ID", referencedColumnName = "VACCINE_ID")
  private Vaccine vaccineByVaccineId;

}
