package fa.training.models.dto;

import jakarta.validation.constraints.FutureOrPresent;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.PastOrPresent;
import lombok.*;

import java.sql.Date;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VaccineDTO {
    @NotBlank
    private String vaccineId;

    private String status;

    @NotBlank
    private String vaccineName;

    @NotBlank
    private String vaccineType;

    private Integer numberOfInjection;

    @NotBlank
    private String usage;

    @NotBlank
    private String indication;

    @NotBlank
    private String contraindication;
    
    private Date timeOfBeginningNextInjection;

    private Date timeOfEndingNextInjection;

    @NotBlank
    private String origin;

}
