package fa.training.models.dto;

import jakarta.validation.constraints.FutureOrPresent;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.PastOrPresent;
import lombok.*;

import java.sql.Date;
import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InjectionScheduleWithStatusDTO {

    private String injectionScheduleId;

    @NotBlank
    private String vaccine;

    
    private Date startDate;

    @FutureOrPresent
    private Date endDate;

    @NotBlank
    private String place;

    @NotBlank
    private String status;

    private String note;

}
