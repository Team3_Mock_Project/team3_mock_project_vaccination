package fa.training.models.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ListCustomerReportDTO {

  private String customerId;
  private String fullName;
  private String dateOfBirth;
  private String address;
  private String identityCard;
  private String numberOfInjection;

}
