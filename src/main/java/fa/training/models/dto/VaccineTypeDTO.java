package fa.training.models.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class VaccineTypeDTO {

  private String vaccineTypeId;
  private String code;
  private String vaccineTypeName;
  private String description;
  private Integer status;
  private String image;


}
