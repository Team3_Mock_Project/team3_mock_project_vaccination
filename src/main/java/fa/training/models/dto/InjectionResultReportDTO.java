package fa.training.models.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InjectionResultReportDTO {

  private String injectionResultId;
  private String vaccineName;
  private String prevention;
  private String fullName;
  private String injectionDate;
  private Integer numberOfInjection;
}
