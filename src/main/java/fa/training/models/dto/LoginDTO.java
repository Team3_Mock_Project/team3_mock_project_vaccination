package fa.training.models.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginDTO {

  @NotBlank(message = "Username must be not empty!")
  private String username;

  @NotBlank(message = "Password must be not empty!")
  private String password;
}
