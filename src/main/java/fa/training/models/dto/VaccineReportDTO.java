package fa.training.models.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class VaccineReportDTO {

  private String vaccineId;
  private String vaccineName;
  private String vaccineType;
  private Integer numOfInject;
  private String beginNextInjectDate;
  private String endNextInjectDate;
  private String origin;
}
