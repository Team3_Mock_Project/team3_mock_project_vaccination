package fa.training.models.dto;

import fa.training.models.entity.Customer;
import lombok.*;

import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InjectionResultDTO {
    private UUID injectionResultId;
    private Customer customer;
    private String vaccineName;
    private String prevention;
    private Integer numberOfInject;
    private String  dateOfInject;
    private String nextInjectDate;
}
