package fa.training.models.dto;

import lombok.*;
import org.springframework.web.bind.annotation.GetMapping;

import java.sql.Date;
import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDTO {

    private UUID customerId;

    private String fullName;

    private Date dateOfBirth;

    private String gender;

    private String identityCard;

    private String address;

    private String username;

    private String password;

    private String passwordConfirm;

    private String email;

    private String phone;
}
