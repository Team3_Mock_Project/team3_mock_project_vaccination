package fa.training.controllers;

import fa.training.models.dto.VaccineDTO;
import fa.training.models.entity.Vaccine;
import fa.training.services.VaccineService;
import fa.training.services.VaccineTypeService;
import jakarta.validation.Valid;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Controller
@Validated
public class VaccineController {

    @Autowired
    final VaccineService vaccineService;

    @Autowired
    final VaccineTypeService vaccineTypeService;

    public VaccineController(VaccineService vaccineService, VaccineTypeService vaccineTypeService) {
        this.vaccineService = vaccineService;
        this.vaccineTypeService = vaccineTypeService;
    }

    @GetMapping("/list/vaccine")
    public String vaccineList(VaccineDTO vaccineDTO){
        return "vaccine";
    }

    @GetMapping("/create/vaccine")
    public String createVaccineUI(Model model){
        Set<String> vaccineTypeNames = vaccineTypeService.getAllVaccineTypeNames();
        model.addAttribute("vaccineTypeNames", vaccineTypeNames);
        model.addAttribute("vaccineDTO", new VaccineDTO());
        return "create-vaccine";
    }

    @PostMapping("/create/vaccine")
    public String createVaccine(@Valid
                                @ModelAttribute("vaccineDTO") VaccineDTO vaccineDTO,
                                BindingResult result,
                                Model model,
                                RedirectAttributes attributes) {
        if (result.hasErrors()){
            return "create-vaccine";
        }
        Optional<Vaccine> vaccine = vaccineService.findById(UUID.fromString(vaccineDTO.getVaccineId()));
        if (vaccine.isPresent()){
            model.addAttribute("message", "This vaccine id is already exist.");
            return "create-vaccine";
        } else {
            vaccineService.createNew(vaccineDTO);
            attributes.addFlashAttribute("message", "Create new vaccine successfully!");
            return "redirect:/vaccine";
        }
    }

    @GetMapping("/update/vaccine")
    public String updateVaccineUI(Model model){
        Set<String> vaccineTypes = vaccineTypeService.getAllVaccineTypeNames();
        model.addAttribute("vaccineTypes", vaccineTypes);
        model.addAttribute("vaccineDTO", new VaccineDTO());
        return "update-vaccine";
    }

    @PostMapping("/update/vaccine")
    public String updateVaccine(@Valid
                                @ModelAttribute("vaccineDTO") VaccineDTO vaccineDTO,
                                BindingResult result,
                                Model model){
        if (result.hasErrors()){
            return "update-vaccine";
        }
        vaccineService.update(vaccineDTO);
        model.addAttribute("message", "Update vaccine successfully");
        return "redirect:/vaccine";
    }

}
