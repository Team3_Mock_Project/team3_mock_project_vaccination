package fa.training.controllers;

import fa.training.models.dto.LoginDTO;
import fa.training.models.entity.Employee;
import fa.training.services.EmployeeService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LoginController {

  @Autowired
  EmployeeService employeeService;

  @GetMapping("/login")
  public String getLoginUI(LoginDTO loginDTO) {
    return "login";
  }

  @PostMapping("/login")
  public String login(
      @Valid
      @ModelAttribute LoginDTO loginDTO,
      BindingResult result,
      Model model
  ) {
    if (result.hasErrors()) {
      return "login";
    }
    Employee employee = employeeService.findAdminByUsernameAndPassword(loginDTO.getUsername(),
        loginDTO.getPassword());
    if (employee != null) {
      return "redirect:/home";
    } else {
      model.addAttribute("error",
          "Sorry, your username or password is incorrect. Please try again!");
      return "login";
    }
  }
}
