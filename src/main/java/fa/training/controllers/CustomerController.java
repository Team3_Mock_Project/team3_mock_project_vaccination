package fa.training.controllers;

import fa.training.models.dto.CustomerDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CustomerController {

    @GetMapping("/list/customer")
    public String listCustomer(){
        return "customer";
    }

    @GetMapping("/create/customer")
    public String createCustomerUI(){
        return "create-customer";
    }

    @GetMapping("/update/customer")
    public String updateCustomerUI(){
        return "update-customer";
    }
}
