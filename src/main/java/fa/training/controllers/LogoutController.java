package fa.training.controllers;

import org.springframework.stereotype.Controller;

@Controller
public class LogoutController {

    public String logout(){
        return "logout";
    }
}
