package fa.training.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class VaccineTypeController {

  @GetMapping("/list/vaccineType")
  public String getVaccineTypeListUI() {
    return "list-vaccine-type";
  }

  @GetMapping("/create/vaccineType")
  public String getCreateVaccineTypeListUI() {
    return "create-vaccine-type";
  }


}
