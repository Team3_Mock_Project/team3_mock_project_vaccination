package fa.training.controllers;

import fa.training.services.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class NewController {

  @Autowired
  NewsService newsService;

  @GetMapping("list/news")
  public String getNewListUI() {
    return "list-news";
  }

  @GetMapping("create/news")
  public String getCreateNewsUI() {
    return "create-news";
  }

  @GetMapping("update/news")
  public String getUpdateNewsUI() {
    return "update-news";
  }


}
