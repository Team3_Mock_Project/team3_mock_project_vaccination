package fa.training.controllers;

import fa.training.services.InjectionResultService;
import fa.training.services.VaccineTypeService;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ReportInjectionResultController {

  @Autowired
  InjectionResultService injectionResultService;

  @Autowired
  VaccineTypeService vaccineTypeService;

  @GetMapping("/report/injectionResult")
  public String getReportUI(Model model) {
    List<Integer> years = injectionResultService.getAllYearOfInjectionResult();
    Set<String> vaccineTypes = vaccineTypeService.getAllVaccineTypeNames();
    model.addAttribute("years", years);
    model.addAttribute("vaccineTypes", vaccineTypes);
    return "report-injectionResult";
  }

}
