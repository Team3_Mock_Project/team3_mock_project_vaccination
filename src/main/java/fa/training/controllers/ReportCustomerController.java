package fa.training.controllers;

import fa.training.services.InjectionResultService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ReportCustomerController {

  @Autowired
  InjectionResultService injectionResultService;

  @GetMapping("/report/customerReport")
  public String getReportUI(Model model) {
    List<Integer> years = injectionResultService.getAllYearOfInjectionResult();
    model.addAttribute("years", years);
    return "report-customer";
  }
}
