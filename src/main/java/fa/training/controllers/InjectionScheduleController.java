package fa.training.controllers;

import fa.training.models.dto.InjectionScheduleWithStatusDTO;
import fa.training.models.entity.Vaccine;
import fa.training.services.InjectionService;
import fa.training.services.VaccineService;
import java.text.ParseException;
import java.util.List;
import java.util.Set;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class InjectionScheduleController {

  @Autowired
  final VaccineService vaccineService;

  @Autowired
  final InjectionService injectionService;

  public InjectionScheduleController(VaccineService vaccineService,
      InjectionService injectionService) {
    this.vaccineService = vaccineService;
    this.injectionService = injectionService;
  }

  @GetMapping("/list/injection/schedule")
  public String injectionScheduleUI() {
    return "injection-schedule";
  }


  @GetMapping("create/injection/schedule")
  public String addNewScheduleUI(Model model) {
    List<Vaccine> vaccines = vaccineService.getAll();
    model.addAttribute("vaccines", vaccines);
    model.addAttribute("withStatusDTO", new InjectionScheduleWithStatusDTO());
    return "create-injection-schedule";
  }

  @PostMapping("create/injection/schedule")
  public String addNewSchedule(@Valid
                               @ModelAttribute("withStatusDTO") InjectionScheduleWithStatusDTO withStatusDTO,
                               BindingResult result,
                               Model model,
                               RedirectAttributes attributes) throws ParseException {

    if (result.hasErrors()){
      return "create-injection-schedule";
    }

    if (withStatusDTO.getStartDate().after(withStatusDTO.getEndDate())){
      model.addAttribute("message", "Start date must be less than end date.");
    }

    injectionService.createNew(withStatusDTO);
    attributes.addFlashAttribute("message",
        "Create a new injection schedule success.");
    return "redirect:/list/injection/schedule";
  }

  @GetMapping("update/injection/schedule")
  public String updateInjectionScheduleUI(
      Model model) {
    List<Vaccine> vaccines = vaccineService.getAll();
    Set<String> statuses = injectionService.getAllStatus();
    model.addAttribute("statuses", statuses);
    model.addAttribute("vaccines", vaccines);
    model.addAttribute("withStatusDTO", new InjectionScheduleWithStatusDTO());
    return "update-injection-schedule";
  }
  @PostMapping("update/injection/schedule")
  public String updateInjectionSchedule(@Valid
                                        @ModelAttribute("withStatusDTO") InjectionScheduleWithStatusDTO withStatusDTO,
                                        BindingResult result,
                                        Model model){

    if (result.hasErrors()){
      model.addAttribute("message", "Start date must be less than end date.");
      return "update-injection-schedule";
    } else {
      injectionService.update(withStatusDTO);
      model.addAttribute("message", "Update injection schedule success.");
      return "redirect:/list/injection/schedule";
    }
  }


}


