package fa.training.controllers;
import fa.training.models.dto.InjectionResultDTO;
import fa.training.services.*;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.UUID;

@Controller
public class InjectionResultController {
    @Autowired
    private InjectionResultService injectionResultService;
    @Autowired
    VaccineService vaccineService;
    @Autowired
    CustomerService customerService;
    @Autowired
    VaccineTypeService vaccineTypeService;
    public InjectionResultController(InjectionResultService injectionResultService,
                                     VaccineService vaccineService,
                                     CustomerService customerService) {
        this.injectionResultService = injectionResultService;
        this.vaccineService = vaccineService;
        this.customerService = customerService;
    }
    @GetMapping("/injection-result")
    public String injectionResultUI() {
        return "injection-result";
    }
    @GetMapping("create/injection-result")
    public String addInjectionResultUI(Model model) {
        model.addAttribute("injectionResultDTO", new InjectionResultDTO());
        model.addAttribute("customers", customerService.findAllCustomer());
        model.addAttribute("vaccinesTypes", injectionResultService.getAllPreventions());
        model.addAttribute("injectionPlaces", injectionResultService.getAllPlaces());
        return "create-injection-result";


    }
    @PostMapping("/create/injection-result")
    public String addNewInjectionResult(@Valid @ModelAttribute("injectionResultDTO") InjectionResultDTO injectionResultDTO,
                                        BindingResult result,
                                        RedirectAttributes attributes){
        if(result.hasErrors()){
            return "create-injection-result";
        }
        injectionResultService.createNew(injectionResultDTO);
        attributes.addFlashAttribute("message", "Create new injection result successfully!");
        return "redirect:/injection-result";
    }
    @GetMapping("/update/injection-result")
    public String updateInjectResultUI(Model model) {
        model.addAttribute("customers", customerService.findAllCustomer());
        model.addAttribute("vaccines", vaccineService.getAll());
        model.addAttribute("injectionResultDTO", new InjectionResultDTO());
        return "create-injection-result";
    }
    @PutMapping("/update/injection-result")
    public String updateInjectionResult(@Valid @ModelAttribute("injectionResultDTO") InjectionResultDTO injectionResultDTO,
                                        BindingResult result,
                                        Model model) {
        if(result.hasErrors()){
            return "update-injection-result";
        }
        injectionResultService.update(injectionResultDTO);
        model.addAttribute("message", "Update successfully!");
        return "redirect:/injection-result";
    }
    @DeleteMapping("/delete/injection-result/{id}")
    public String deleteInjectionResult(@PathVariable("injectionResultId") UUID uuid) {
        injectionResultService.deleteInjectionResultById(uuid);
        return "redirect:/inject-result";
    }

}
