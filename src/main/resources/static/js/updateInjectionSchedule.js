document.addEventListener('DOMContentLoaded', function () {
  const storedData = sessionStorage.getItem('selectedData');
  if (storedData) {
    const jsonData = JSON.parse(storedData);
    $('#injectionScheduleId').val(jsonData.injectionScheduleId);
    $('#vaccine').val(jsonData.vaccine);
    $('#startDate').val(jsonData.startDate);
    $('#endDate').val(jsonData.endDate);
    $('#status').val(jsonData.status);
    $('#place').val(jsonData.place);
    $('#note').val(jsonData.note);
    console.log(jsonData);
  }
})
