$(document).ready(function () {
  $('#reportInjectionResult').DataTable({
    ajax: {
      url: 'http://localhost:8888/api/injectionResult/report',
      dataSrc: ''
    },
    "bDestroy": true,
    columns: [
      {data: 'injectionResultId'},
      {data: 'vaccineName'},
      {data: 'prevention'},
      {data: 'fullName'},
      {data: 'injectionDate'},
      {data: 'numberOfInjection'}
    ]
  });
});

function submitForm() {
  const fromDate = document.getElementById('from').value;
  const toDate = document.getElementById('to').value;
  const prevention = document.getElementById('prevent').value;
  const vaccineType = document.getElementById('vaccineSelected').value;

  $.ajax({
    type: 'GET',
    url: `http://localhost:8888/api/injectionResult/filterReport?fromInjectDate=${fromDate}
    &toInjectDate=${toDate}&prevention=${prevention}&vaccineType=${vaccineType}`,
    dataType: "json",
    success: function (data) {
      const table = $('#reportInjectionResult').DataTable();

      table.clear().rows.add(data).draw();
    },
    error: function (error) {
      console.error('Error submitting form');
    }
  });
}