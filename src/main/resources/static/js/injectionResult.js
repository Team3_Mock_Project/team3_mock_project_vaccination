document.getElementById('selectAll').addEventListener('click', function () {
    let checkboxes = document.querySelectorAll('.select-checkbox');
    checkboxes.forEach(function (checkbox) {
        checkbox.checked = document.getElementById('selectAll').checked;
    });
});

$(document).ready(function () {
    $('#listNews').DataTable({
        ajax: {
            url: 'http://localhost:8888/api/news/list',
            dataSrc: ''
        },
        columns: [
            {
                data: null,
                orderable: false,
                render: function (data, type, row) {
                    return '<input type="checkbox" row-id-attr="'
                        + data.newsId + '" class="select-checkbox" value="'
                        + row.newsId + '">';
                }
            },
            {data: 'title'},
            {data: 'content'},
            {data: 'postDate'}
        ]
    });
});