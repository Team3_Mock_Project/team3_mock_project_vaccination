function openModal(myModal) {
    $(myModal).modal('show');
}

function showToast(myToast) {
    $(myToast).toast('show');
}

// let selectedGender;
// $(document).ready(function() {
//     $('input[name="gender"]').change(function() {
//         selectedGender = $('input[name="gender"]:checked').val();
//         console.log(selectedGender); // Sử dụng giá trị này theo nhu cầu của bạn
//     });
// });

document.addEventListener('DOMContentLoaded', function() {
    const code = document.getElementById('code');
    const refreshButton = document.getElementById('refreshButton');

    function generateRandomCode() {
        // Tạo một số ngẫu nhiên, bạn có thể thay đổi cách sinh số tùy theo yêu cầu
        const randomCode = Math.floor(Math.random() * 1000000).toString().padStart(6, '0'); // Sinh số ngẫu nhiên 6 chữ số
        code.value = randomCode;
    }
    // Sinh số ngẫu nhiên khi trang tải
    generateRandomCode();

    // Gán sự kiện click cho nút refresh
    refreshButton.addEventListener('click', generateRandomCode);
});

function saveCustomer() {
    if (validateCustomer()){
        openModal('#confirmSaveCustomerModal');
    }
    let myData = {
        fullName: $('#fullName').val().trim(),
        dateOfBirth: $('#dateOfBirth').val().trim(),
        gender:  $('input[name="gender"]').val().trim(),
        identityCard: $('#identityCard').val().trim(),
        address: $('#address').val().trim(),
        username: $('#username').val().trim(),
        password: $('#password').val().trim(),
        passwordConfirm: $('#passwordConfirm').val().trim(),
        email: $('#email').val().trim(),
        phone: $('#phone').val().trim(),
        capcha: $('#capcha').val().trim(),
        code: $('#code').val(),
    };
    $('#btn-save-customer').on('click', function () {

        $.ajax({
            url: 'http://localhost:8888/api/create/customer',
            method: 'POST',
            data: JSON.stringify(myData),
            contentType: 'application/json',
            success: function (data) {
                $('#confirmSaveCustomerModal').modal('hide');
                window.location.href = 'http://localhost:8888/list/customer?showToast=create';
            },
            error: function (error) {
                console.error('Error saving news');
            }
        });
    });
}

function validateCustomer(){
    let fullName = $('#fullName').val();
    let dateOfBirth = $('#dateOfBirth').val();
    let gender = $('input[name="gender"]').val();
    let identityCard = $('#identityCard').val();
    let address = $('#address').val();
    let username = $('#username').val();
    let password = $('#password').val();
    let passwordConfirm = $('#passwordConfirm').val();
    let email = $('#email').val();
    let phone = $('#phone').val();
    let capcha = $('#capcha').val();
    let code = $('#code').val();

    let fullNameError = $('#fullNameError');
    let dateOfBirthError = $('#dateOfBirthError');
    let genderError = $('#genderError');
    let identityCardError = $('#identityCardError');
    let addressError = $('#addressError');
    let usernameError = $('#usernameError');
    let passwordError = $('#passwordError');
    let passwordCheck = $('#passwordCheck');
    let emailError = $('#emailError');
    let phoneError = $('#phoneError');
    let capchaError= $('#checkCaptcha');

    fullNameError.text('');
    dateOfBirthError.text('');
    genderError.text('');
    identityCardError.text('');
    addressError.text('');
    usernameError.text('');
    passwordError.text('');
    passwordCheck.text('');
    emailError.text('');
    phoneError.text('');
    capchaError.text('');

    if (fullName === ''){
        fullNameError.text('Please fill out full name');
        return false;
    }
    if (dateOfBirth === ''){
        dateOfBirthError.text('Please select date of birth');
        return false;
    }
    if (gender === ''){
        genderError.text('Choose gender please');
        return false;
    }
    if (identityCard === ''){
        identityCardError.text('Fill out identity card number please');
        return false;
    }
    if (address === ''){
        addressError.text('Pick a location please');
        return false;
    }
    if (username === ''){
        usernameError.text('Fill out username please');
        return false;
    }
    if (password === ''){
        passwordError.text('Fill out password please');
        return false;
    }
    if (passwordConfirm !== password){
        passwordCheck.text("Password doesn't match");
        return false;
    }
    if (email === ''){
        emailError.text('Fill out email please');
        return false;
    }
    if (phone === ''){
        phoneError.text('Fill out phone please');
        return false;
    }
    if (capcha !== code){
        capchaError.text("Captcha doesn't match");
        console.log(capchaError);
        return false;
    }
    return true;
}

