document.getElementById('selectAll').addEventListener('click', function () {
  let checkboxes = document.querySelectorAll('.select-checkbox');
  checkboxes.forEach(function (checkbox) {
    checkbox.checked = document.getElementById('selectAll').checked;
  });
});

$(document).ready(function () {
  $('#listVaccineType').DataTable({
    ajax: {
      url: 'http://localhost:8888/api/vaccineType/list',
      dataSrc: ''
    },
    columns: [
      {
        data: null,
        orderable: false,
        render: function (data, type, row) {
          return '<input type="checkbox" row-id-attr="'
              + data.vaccineTypeId + '" class="select-checkbox" value="'
              + row.vaccineTypeId + '">';
        }
      },
      {data: 'code'},
      {data: 'vaccineTypeName'},
      {data: 'description'},
      {
        data: null,
        render: function (data, type, row) {
          if (row.status === 1) {
            return 'Active';
          } else {
            return 'In-Active';
          }
        }
      }
    ]
  });
});

function getFirstSelectedId() {
  let selectedIds = [];
  $('.select-checkbox').each(function () {
    if ($(this).is(':checked')) {
      selectedIds.push($(this).val());
    }
  });

  if (selectedIds.length === 0) {
    alert("Please select at least one checkbox.");
    return null;
  }

  return selectedIds[0];
}

// Function to open modal
function openModal(myModal) {
  $(myModal).modal('show');
}

function showToast(myToast) {
  $(myToast).toast('show');
}

function makeInActiveVaccineType() {
  let id = getFirstSelectedId();
  if (id !== null) {
    openModal('#confirmInActiveModal');
    $('#btn-make-inactive').on('click', function () {
      $.ajax({
        url: '/api/vaccineType/update/' + id,
        type: 'POST',
        success: function (data) {
          $('#confirmInActiveModal').modal('hide');
          $('#listVaccineType').DataTable().ajax.reload();
          showToast("#toastInactiveSuccess");
        },
        error: function (error) {
          console.error('Error deleting news');
        }
      });
    });
  }
}

$(document).ready(function () {
  const urlParams = new URLSearchParams(window.location.search);
  if (urlParams.has('showToast') && urlParams.get('showToast') === 'true') {
    showToast("#toastCreateVaccineTypeSuccess");
  }
});

