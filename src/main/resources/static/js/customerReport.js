$(document).ready(function () {
  $('#reportCustomer').DataTable({
    ajax: {
      url: 'http://localhost:8888/api/customer/report',
      dataSrc: ''
    },
    "bDestroy": true,
    columns: [
      {data: 'customerId'},
      {data: 'fullName'},
      {data: 'dateOfBirth'},
      {data: 'address'},
      {data: 'identityCard'},
      {data: 'numberOfInjection'}
    ]
  });
});

function submitForm() {
  const fromDate = document.getElementById('from').value;
  const toDate = document.getElementById('to').value;
  const fullName = document.getElementById('name').value;
  const address = document.getElementById('adr').value;

  $.ajax({
    type: 'GET',
    url: `http://localhost:8888/api/customer/filterReport?fromDate=${fromDate}
    &toDate=${toDate}&fullName=${fullName}&address=${address}`,
    dataType: "json",
    success: function (data) {
      const table = $('#reportCustomer').DataTable();

      table.clear().rows.add(data).draw();
    },
    error: function (error) {
      console.error('Error submitting form');
    }
  });
}


