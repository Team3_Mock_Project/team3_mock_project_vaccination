function openModal(myModal) {
  $(myModal).modal('show');
}

function showToast(myToast) {
  $(myToast).toast('show');
}

function saveVaccineType() {
  openModal('#confirmSaveVaccineTypeModal');
  const imageInput = document.getElementById("image");
  const image = imageInput.files[0].name;
  const status = document.getElementById("status");
  let myData = {
    code: $('#code').val().trim(),
    vaccineTypeName: $('#vaccineTypeName').val().trim(),
    status: status.value,
    description: $('#description').val().trim(),
    image: image
  };
  $('#btn-save-vaccineType').on('click', function () {

    $.ajax({
      url: 'http://localhost:8888/api/vaccineType/create',
      method: 'POST',
      data: JSON.stringify(myData),
      contentType: 'application/json',
      success: function (data) {
        $('#confirmSaveNewsModal').modal('hide');
        window.location.href = 'http://localhost:8888/list/vaccineType?showToast=true';
      },
      error: function (error) {
        console.error('Error saving news');
      }
    });
  });
}

function toggleCheckboxValue() {
  var checkbox = document.getElementById('status');

  if (checkbox.checked) {
    return checkbox.value = 1;
  } else {
    return checkbox.value = 0;
  }
}

