let tableVaccine = new DataTable('#example');

if (document.getElementById('selectAllVaccine') != null && document.getElementById('selectAllVaccine') != undefined) {
    document.getElementById('selectAllVaccine').addEventListener('click', function () {
        let checkboxes = document.querySelectorAll('.select-checkbox');
        checkboxes.forEach(function (checkbox) {
            checkbox.checked = document.getElementById('selectAllVaccine').checked;
        });
    });
}

$(document).ready(function () {
    $('#tableVaccine').DataTable({
        ajax: {
            url: "http://localhost:8888/api/vaccine",
            dataSrc: ''
        },
        "bDestroy": true,
        columns: [
            {
                data: null,
                orderable: false,
                render: function (data, type, row) {
                    return '<input type="checkbox" value="' + data.vaccineId + '" class="select-checkbox">';
                }
            },
            {data: 'vaccineId'},
            {data: 'vaccineName'},
            {data: 'vaccineType'},
            {
                data: null,
                orderable: false,
                render: function (data, type, row) {
                    return '<span class="d-flex justify-content-start">' + data.numberOfInjection + '</span>';
                }
            },
            {data: 'origin'},
            {data: 'status'}
        ]
    });
});

//     document.getElementById('makeInactiveBtn').addEventListener('click', function () {
//         //get all clicked checkbox
//         let selectedCheckboxes = document.querySelectorAll('input[type="checkbox"]:checked');
//
//         if (selectedCheckboxes.length > 0) {
//             // Hiển thị cửa sổ thông báo với SweetAlert2
//             Swal.fire({
//                 title: 'Are you sure to make this In-Active?',
//                 icon: 'warning',
//                 showCancelButton: true,
//                 confirmButtonText: 'Yes',
//                 cancelButtonText: 'No'
//             }).then((result) => {
//                 selectedCheckboxes.forEach(checkbox => {
//                     checkbox.closest('tr').querySelector('#status').textContent = 'In-Active';
//                     checkbox.checked = false;
//                 });
//             });
//         } else {
//             alert("Please select at least 1 checkbox")
//         }
//     })
// });


// document.getElementById('addNewVaccine').addEventListener("click", function () {
//     let vaccineData = {
//         vaccineDTO: {
//             vaccineId: document.getElementById('vaccineId').value,
//             status: document.getElementById("status").value,
//             vaccineName: document.getElementById("vaccineName").value,
//             vaccineType: document.getElementById('vaccineType').value,
//             numberOfInject: document.getElementById('numberOfInject').value,
//             usage: document.getElementById('usage').value,
//             indication: document.getElementById('indication').value,
//             contraindication: document.getElementById('contraindication').value,
//             timeOfBeginningNextInjection: new Date(document.getElementById('timeOfBeginningNextInjection').value),
//             timeOfEndingNextInjection: new Date(document.getElementById('timeOfEndingNextInjection').value),
//             origin: document.getElementById('origin').value
//         },
//     }
//
//     $.ajax({
//         url: "http://localhost:8888/api/vaccine",
//         method: "POST",
//         contentType: "application/json",
//         data: JSON.stringify(vaccineData),
//         success: function (response) {
//             console.log(response)
//             alert("Add new vaccine success!");
//             window.location.href = "http://localhost:8888/vaccine";
//         },
//         error: function (xhr, status, error) {
//             console.log(error)
//         }
//     });
// });


function validateTimeInput() {
    let beginTime = document.getElementById('timeOfBeginningNextInjection').value;
    let endTime = document.getElementById('timeOfEndingNextInjection').value;
    let beginTimeAsDate = Date.parse(beginTime);
    let endTimeAsDate = Date.parse(endTime);
    let currentDate = new Date().getTime();
    console.log(beginTimeAsDate, endTimeAsDate, currentDate)
    let activeCheckbox = document.getElementById('status');
    let beginTimeError = document.getElementById('beginningTimeError');
    let endTimeError = document.getElementById('endingTimeError');
    let errorCheckbox = document.getElementById('error-message');
    beginTimeError.innerHTML = '';
    endTimeError.innerHTML = '';
    errorCheckbox.innerHTML = '';

    if (beginTimeAsDate > endTimeAsDate) {
        beginTimeError.innerHTML = "The beginning time must be less than the ending time";
        return false;
    }
    if ((beginTimeAsDate > currentDate || endTimeAsDate < currentDate) && activeCheckbox.checked) {
        errorCheckbox.innerHTML = "It's not the time for this vaccine to active";
        return false;
    }
    return true;
}

function getFirstSelectedId() {
    let selectedIds = [];
    $('.select-checkbox').each(function () {
        if ($(this).is(':checked')) {
            selectedIds.push($(this).val());
        }
    });

    if (selectedIds.length === 0) {
        alert("Please select at least one checkbox.");
        return null;
    }

    return selectedIds[0];
}

function get_detail_vaccine() {

    let id = getFirstSelectedId(); // Assuming you only need the first selected ID

    $.ajax({
        type: 'GET',
        url: 'http://localhost:8888/api/update/vaccine/' + id, // Pass all selected IDs
        dataType: "json",
        success: function (data) {
            console.log(data);
            if (data) {
                window.location.href = 'http://localhost:8888/update/vaccine';
                $('#vaccineId').val(data.vaccineId);
                $('#status').val(data.status);
                $('#vaccineName').val(data.vaccineName);
                $('#vaccineType').val(data.vaccineType);
                $('#numberOfInjection').val(data.numberOfInjection);
                $('#usage').val(data.usage);
                $('#indication').val(data.indication);
                $('#contraindication').val(data.contraindication);
                $('#timeOfBeginningNextInjection').val(data.timeOfBeginningNextInjection);
                $('#timeOfEndingNextInjection').val(data.timeOfEndingNextInjection);
                $('#origin').val(data.origin);

                // Store fetched data in sessionStorage
                sessionStorage.setItem("selectedData", JSON.stringify(data));
            } else {
                alert("No data found for the selected ID(s).");
            }
        },
        error: function (xhr, status, error) {
            console.error(error);
            alert("Error occurred while fetching data.");
        }
    });
}

function openModal(myModal) {
    $(myModal).modal('show');
}

function showToast(myToast) {
    $(myToast).toast('show');
}

function makeInActiveVaccine() {
    let id = getFirstSelectedId();
    if (id !== null) {
        openModal('#confirmInActiveModal');
        $('#btn-make-inactive').on('click', function () {
            $.ajax({
                url: '/api/update/status/' + id,
                type: 'POST',
                success: function (data) {
                    $('#confirmInActiveModal').modal('hide');
                    $('#tableVaccine').DataTable().ajax.reload();
                    showToast("#toastInactiveSuccess");
                },
                error: function (error) {
                    console.error('Error deleting news');
                }
            });
        });
    }
}

function getCheckbox(){
    let checkbox = document.getElementById('status');
    if (checkbox.checked) {
        return checkbox.value = "Active";
    } else {
        return checkbox.value = "In-Active";
    }
}









