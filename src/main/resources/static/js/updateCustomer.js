const storedData = sessionStorage.getItem('selectedData');
const jsonData = JSON.parse(storedData);

function openModal(myModal) {
    $(myModal).modal('show');
}
document.addEventListener('DOMContentLoaded', function () {
    if (storedData) {
        $('#customerId').val(jsonData.customerId);
        $('#fullName').val(jsonData.fullName);
        $('#dateOfBirth').val(jsonData.dateOfBirth);
        if (jsonData.gender === 'Male'){
            $('#male').prop('checked', true);
        } else if (jsonData.gender === 'Female'){
            $('#female').prop('checked', true);
        }
        $('#identityCard').val(jsonData.identityCard);
        $('#address').val(jsonData.address);
        $('#username').val(jsonData.username);
        $('#password').val(jsonData.password);
        $('#email').val(jsonData.email);
        $('#phone').val(jsonData.phone);
        console.log(jsonData);
    }
})

let selectedGender;

$(document).ready(function() {
    $('input[name="gender"]').change(function() {
        selectedGender = $('input[name="gender"]:checked').val();
        console.log(selectedGender);
    });
});

function updateCustomer(){
    if (validateCustomer()){
        openModal('#confirmUpdateCustomerModal');
    }
    let myData = {
        customerId: $('#customerId').val(),
        fullName: $('#fullName').val().trim(),
        dateOfBirth: $('#dateOfBirth').val().trim(),
        gender:  $('input[name="gender"]').val(),
        identityCard: $('#identityCard').val().trim(),
        address: $('#address').val().trim(),
        username: $('#username').val().trim(),
        password: $('#password').val().trim(),
        passwordConfirm: $('#passwordConfirm').val().trim(),
        email: $('#email').val().trim(),
        phone: $('#phone').val().trim(),
        capcha: $('#capcha').val().trim(),
        code: $('#code').val().trim(),
    };
    $('#btn-update-customer').on('click', function () {

        $.ajax({
            url: 'http://localhost:8888/api/update/customer',
            method: 'PUT',
            data: JSON.stringify(myData),
            contentType: 'application/json',
            success: function (data) {
                console.log(data);
                $('#confirmSaveCustomerModal').modal('hide');
                window.location.href = 'http://localhost:8888/list/customer?showToast=update';
            },
            error: function (error) {
                console.log('Error saving customer');
            }
        });
    });
}

function validateCustomer(){
    let fullName = $('#fullName').val();
    let dateOfBirth = $('#dateOfBirth').val();
    let gender = $('input[name="gender"]').val();
    let identityCard = $('#identityCard').val();
    let address = $('#address').val();
    let username = $('#username').val();
    let password = $('#password').val();
    let passwordConfirm = $('#passwordConfirm').val();
    let email = $('#email').val();
    let phone = $('#phone').val();
    let capcha = $('#capcha').val();
    let code = $('#code').val();

    let fullNameError = $('#fullNameError');
    let dateOfBirthError = $('#dateOfBirthError');
    let genderError = $('#genderError');
    let identityCardError = $('#identityCardError');
    let addressError = $('#addressError');
    let usernameError = $('#usernameError');
    let passwordError = $('#passwordError');
    let passwordCheck = $('#passwordCheck');
    let emailError = $('#emailError');
    let phoneError = $('#phoneError');
    let capchaError= $('#checkCaptcha');

    fullNameError.text('');
    dateOfBirthError.text('');
    genderError.text('');
    identityCardError.text('');
    addressError.text('');
    usernameError.text('');
    passwordError.text('');
    passwordCheck.text('');
    emailError.text('');
    phoneError.text('');
    capchaError.text('');

    if (fullName === ''){
        fullNameError.text('Please fill out full name');
        return false;
    }
    if (dateOfBirth === ''){
        dateOfBirthError.text('Please select date of birth');
        return false;
    }
    if (gender === ''){
        genderError.text('Choose gender please');
        return false;
    }
    if (identityCard === ''){
        identityCardError.text('Fill out identity card number please');
        return false;
    }
    if (address === ''){
        addressError.text('Pick a location please');
        return false;
    }
    if (username === ''){
        usernameError.text('Fill out username please');
        return false;
    }
    if (password === ''){
        passwordError.text('Fill out password please');
        return false;
    }
    if (email === ''){
        emailError.text('Fill out email please');
        return false;
    }
    if (phone === ''){
        phoneError.text('Fill out phone please');
        return false;
    }
    return true;
}