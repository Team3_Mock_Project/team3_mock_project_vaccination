$(document).ready(function () {
  $('#reportVaccine').DataTable({
    ajax: {
      url: 'http://localhost:8888/api/vaccine/report',
      dataSrc: ''
    },
    "bDestroy": true,
    columns: [
      {data: 'vaccineId'},
      {data: 'vaccineName'},
      {data: 'vaccineType'},
      {data: 'numOfInject'},
      {data: 'beginNextInjectDate'},
      {data: 'endNextInjectDate'},
      {data: 'origin'}
    ]
  });
});

function submitForm() {
  const beginDate = document.getElementById('from').value;
  const endDate = document.getElementById('to').value;
  const vaccineType = document.getElementById('vaccineSelected').value;
  const origin = document.getElementById('org').value;

  $.ajax({
    type: 'GET',
    url: `http://localhost:8888/api/vaccine/filterReport?beginDate=${beginDate}
    &endDate=${endDate}&vaccineType=${vaccineType}&origin=${origin}`,
    dataType: "json",
    success: function (data) {
      const table = $('#reportVaccine').DataTable();

      table.clear().rows.add(data).draw();
    },
    error: function (error) {
      console.error('Error submitting form');
    }
  });
}