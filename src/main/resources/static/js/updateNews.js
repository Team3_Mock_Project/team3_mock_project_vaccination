const storedData = sessionStorage.getItem('viewsData');
const jsonData = JSON.parse(storedData);

function openModal(myModal) {
  $(myModal).modal('show');
}

document.addEventListener('DOMContentLoaded', function () {
  $('#newsId').val(jsonData.newsId);
  $('#titleUpdate').val(jsonData.title);
  $('#previewUpdate').val(jsonData.preview);
  $('#contentUpdate').val(jsonData.content);
  console.log(jsonData);
})

function updateNews() {
  openModal('#confirmUpdateNewsModal');
  let myData = {
    newsId: $('#newsId').val(),
    title: $('#titleUpdate').val().trim(),
    preview: $('#previewUpdate').val().trim(),
    content: $('#contentUpdate').val().trim()
  };
  $('#btn-update-news').on('click', function () {

    $.ajax({
      url: 'http://localhost:8888/api/news/update',
      method: 'POST',
      data: JSON.stringify(myData),
      contentType: 'application/json',
      success: function (data) {
        $('#confirmSaveNewsModal').modal('hide');
        window.location.href = 'http://localhost:8888/list/news?showToast=update';
      },
      error: function (error) {
        console.error('Error saving news');
      }
    });
  });
}