document.addEventListener('DOMContentLoaded', function () {
    const storedData = sessionStorage.getItem('selectedData');
    if (storedData) {
        const jsonData = JSON.parse(storedData);
        $('#vaccineId').val(jsonData.vaccineId);
        $('#status').val(jsonData.status);
        $('#vaccineName').val(jsonData.vaccineName);
        $('#vaccineType').val(jsonData.vaccineType);
        $('#numberOfInjection').val(jsonData.numberOfInjection);
        $('#usage').val(jsonData.usage);
        $('#indication').val(jsonData.indication);
        $('#contraindication').val(jsonData.indication);
        $('#timeOfBeginningNextInjection').val(jsonData.timeOfBeginningNextInjection);
        $('#timeOfEndingNextInjection').val(jsonData.timeOfEndingNextInjection);
        $('#origin').val(jsonData.origin);
        console.log(jsonData);
    }
})