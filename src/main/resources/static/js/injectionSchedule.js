
let tableInit = new DataTable('#example');

document.getElementById('selectAll').addEventListener('click', function () {
  let checkboxes = document.querySelectorAll('.select-checkbox');
  checkboxes.forEach(function (checkbox) {
    checkbox.checked = document.getElementById('selectAll').checked;
  });
});

$(document).ready(function () {
  $('#table').DataTable({
    ajax: {
      url: "http://localhost:8888/api/injection/schedule",
      dataSrc: ''
    },
    "bDestroy": true,
    columns: [
      {
        data: null,
        orderable: false,
        render: function (data, type, row) {
          return '<input type="checkbox" row-id-attr="' + data.injectionScheduleId + '" class="select-checkbox" value="' + row.injectionScheduleId + '">';
        }
      },
      {data: 'vaccine'},
      {
        data: null,
        render: function (data, type, row) {
          return 'From ' + '<strong>' + row.startDate + '</strong>' + ' To ' + '<strong>' + row.endDate + '</strong>';
        }
      },
      {data: 'place'},
      {data: 'status'},

      {data: 'note'}
    ]
  });
});

function get_detail() {
  let selectedIds = [];
  $('.select-checkbox').each(function() {
    if ($(this).is(':checked')) {
      selectedIds.push($(this).val());
    }
  });

  if (selectedIds.length === 0) {
    alert("Please select at least one checkbox.");
    return;
  }

  let id = selectedIds[0]; // Assuming you only need the first selected ID

  $.ajax({
    type: 'GET',
    url: 'http://localhost:8888/api/update/injection/schedule/' + id, // Pass all selected IDs
    dataType: "json",
    success: function(data) {
      console.log(data);
      if (data) {
        window.location.href = 'http://localhost:8888/update/injection/schedule';
        $('#injectionScheduleId').val(data.injectionScheduleId);
        $('#vaccine').val(data.vaccine);
        $('#startDate').val(data.startDate);
        $('#endDate').val(data.endDate);
        $('#status').val(data.status);
        $('#place').val(data.place);
        $('#note').val(data.note);

        // Store fetched data in sessionStorage
        sessionStorage.setItem("selectedData", JSON.stringify(data));
      } else {
        alert("No data found for the selected ID(s).");
      }
    },
    error: function(xhr, status, error) {
      console.error(error);
      alert("Error occurred while fetching data.");
    }
  });
}

function update() {
  let selectedIds = [];
  $('.select-checkbox').each(function() {
    if ($(this).is(':checked')) {
      selectedIds.push($(this).val());
    }
  });

  if (selectedIds.length === 0) {
    alert("Please select at least one checkbox.");
    return;
  }

  let id = selectedIds[0]; // Assuming you only need the first selected ID
  $.ajax({
    url: 'http://localhost:8888/api/update/injection/schedule/' + id,
    method: "PUT",
    contentType: "application/json",
    data: JSON.stringify(claimsRequest),
    success: function (response) {
      alert("Save success!");
      window.location.href = "/claimsRequest/draft/" + document.getElementById("staffId").innerText;
      // window.location.href = "/claimsRequest/create/" + lastElement;
    },
    error: function (xhr, status, error) {
      // Handle Error from server
    }
  });
}
