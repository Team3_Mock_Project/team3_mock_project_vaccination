
if (document.getElementById('selectAllVaccine') != null && document.getElementById('selectAllVaccine') != undefined) {
    document.getElementById('selectAllVaccine').addEventListener('click', function () {
        let checkboxes = document.querySelectorAll('.select-checkbox');
        checkboxes.forEach(function (checkbox) {
            checkbox.checked = document.getElementById('selectAllVaccine').checked;
        });
    });
}

$(document).ready(function () {
    $('#tableCustomer').DataTable({
        ajax: {
            url: "http://localhost:8888/api/customer",
            dataSrc: ''
        },
        "bDestroy": true,
        columns: [
            {
                data: null,
                orderable: false,
                render: function (data, type, row) {
                    return '<input type="checkbox" value="' + data.customerId + '" class="select-checkbox text-center">';
                }
            },
            {data: 'fullName'},
            {
                data: null,
                orderable: false,
                render: function (data, type, row) {
                    return '<span class="d-flex justify-content-start">' + data.dateOfBirth + '</span>';
                }
            },
            {
                data: null,
                orderable: false,
                render: function (data, type, row) {
                    return '<span class="d-flex justify-content-start">' + data.gender + '</span>';
                }
            },
            {data: 'address'},
            {
                data: null,
                orderable: false,
                render: function (data, type, row) {
                    return '<span class="d-flex justify-content-start">' + data.identityCard + '</span>';
                }
            },
            {
                data: null,
                orderable: false,
                render: function (data, type, row) {
                    return '<span class="d-flex justify-content-start">' + data.phone + '</span>';
                }
            }
        ]
    });
});

function getFirstSelectedId() {
    let selectedIds = [];
    $('.select-checkbox').each(function () {
        if ($(this).is(':checked')) {
            selectedIds.push($(this).val());
        }
    });

    if (selectedIds.length === 0) {
        alert("Please select at least one checkbox.");
        return null;
    }

    return selectedIds[0];
}

function openModal(myModal) {
    $(myModal).modal('show');
}

function showToast(myToast) {
    $(myToast).toast('show');
}

function confirmDeleteCustomer() {
    let id = getFirstSelectedId();
    console.log(id);
    if (id !== null) {
        openModal('#confirmDeleteCustomerModal');
        $('#btn-delete-customer').off('click').on('click', function () {
            $.ajax({
                url: '/api/delete/customer/' + id,
                type: 'DELETE',
                success: function (data, textStatus, xhr) {
                    console.log('Status:', xhr.status); // Kiểm tra mã trạng thái HTTP
                    if (xhr.status === 204) { // Kiểm tra nếu mã trạng thái là 204 No Content
                        $('#confirmDeleteCustomerModal').modal('hide');
                        $('#tableCustomer').DataTable().ajax.reload();
                        showToast("#toastDeleteCustomerSuccess");
                    }
                },
                error: function (error) {
                    console.error('Error deleting customer:', error);
                }
            });
        });
    }
}

function getDetail(){
    let id = getFirstSelectedId();
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8888/api/update/customer/' + id, // Pass all selected IDs
        dataType: "json",
        success: function (data) {
            console.log(data);
            if (data) {
                window.location.href = 'http://localhost:8888/update/customer';
                $('#customerId').val(data.customerId);
                $('#fullName').val(data.fullName);
                $('#dateOfBirth').val(data.dateOfBirth);
                if (data.gender === 'Male'){
                    $('#male').prop('check', true);
                } else if (data.gender === 'Female'){
                    $('#female').prop('checked', true);
                }
                $('#identityCard').val(data.identityCard);
                $('#address').val(data.address);
                $('#username').val(data.username);
                $('#password').val(data.password);
                $('#email').val(data.email);
                $('#phone').val(data.phone);

                // Store fetched data in sessionStorage
                sessionStorage.setItem("selectedData", JSON.stringify(data));
                console.log(selectedData);
            } else {
                alert("No data found for the selected ID(s).");
            }
        },
        error: function (xhr, status, error) {
            console.error(error);
            alert("Error occurred while fetching data.");
        }
    });
}