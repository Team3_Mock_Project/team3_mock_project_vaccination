document.getElementById('selectAll').addEventListener('click', function () {
  let checkboxes = document.querySelectorAll('.select-checkbox');
  checkboxes.forEach(function (checkbox) {
    checkbox.checked = document.getElementById('selectAll').checked;
  });
});

$(document).ready(function () {
  $('#listNews').DataTable({
    ajax: {
      url: 'http://localhost:8888/api/news/list',
      dataSrc: ''
    },
    columns: [
      {
        data: null,
        orderable: false,
        render: function (data, type, row) {
          return '<input type="checkbox" row-id-attr="'
              + data.newsId + '" class="select-checkbox" value="'
              + row.newsId + '">';
        }
      },
      {data: 'title'},
      {data: 'content'},
      {data: 'postDate'}
    ]
  });
});

function getFirstSelectedId() {
  let selectedIds = [];
  $('.select-checkbox').each(function () {
    if ($(this).is(':checked')) {
      selectedIds.push($(this).val());
    }
  });

  if (selectedIds.length === 0) {
    alert("Please select at least one checkbox.");
    return null;
  }

  return selectedIds[0];
}

// Function to open modal
function openModal(myModal) {
  $(myModal).modal('show');
}

function showToast(myToast) {
  $(myToast).toast('show');
}

function confirmDeleteNews() {
  let id = getFirstSelectedId();
  if (id !== null) {
    openModal('#confirmDeleteNewsModal');
    $('#btn-delete-news').on('click', function () {
      $.ajax({
        url: '/api/news/delete/' + id,
        type: 'GET',
        success: function (data) {
          $('#confirmDeleteNewsModal').modal('hide');
          $('#listNews').DataTable().ajax.reload();
          showToast("#toastDeleteNewsSuccess");
        },
        error: function (error) {
          console.error('Error deleting news');
        }
      });
    });
  }
}

function saveNews() {
  openModal('#confirmSaveNewsModal');
  let myData = {
    title: $('#title').val().trim(),
    preview: $('#preview').val().trim(),
    content: $('#content1').val().trim()
  };
  $('#btn-save-news').on('click', function () {

    $.ajax({
      url: 'http://localhost:8888/api/news/create',
      method: 'POST',
      data: JSON.stringify(myData),
      contentType: 'application/json',
      success: function (data) {
        $('#confirmSaveNewsModal').modal('hide');
        window.location.href = 'http://localhost:8888/list/news?showToast=create';
      },
      error: function (error) {
        console.error('Error saving news');
      }
    });
  });
}

$(document).ready(function () {
  const urlParams = new URLSearchParams(window.location.search);
  if (urlParams.has('showToast') && urlParams.get('showToast') === 'create') {
    showToast("#toastCreateNewsSuccess");
  } else if (urlParams.has('showToast') &&
      urlParams.get('showToast') === 'update') {
    showToast("#toastUpdateNewsSuccess");
  }
});

function getUpdateNewsUI() {
  let id = getFirstSelectedId();
  if (id !== null) {
    $.ajax({
      method: 'GET',
      url: '/api/news/update/' + id,
      dataType: "json",
      success: function (data) {
        if (data) {
          window.location.href = 'http://localhost:8888/update/news';
          sessionStorage.setItem("viewsData", JSON.stringify(data));
        }
      },
      error: function (xhr, status, error) {
        console.error(error);
      }
    });
  }

}




