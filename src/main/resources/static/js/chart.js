async function fetchCustomerReportData(selectedYear) {
  try {
    const response = await fetch(
        `http://localhost:8888/api/customer/statistic?yearSelected=${selectedYear}`);
    return await response.json();
  } catch (error) {
    console.error('Error fetching data:', error);
  }
}

async function fetchInjectionResultReportData(selectedYear) {
  try {
    const response = await fetch(
        `http://localhost:8888/api/injectionResult/statistic?yearSelected=${selectedYear}`);
    return await response.json();
  } catch (error) {
    console.error('Error fetching data:', error);
  }
}

async function fetchVaccineReportData(selectedYear) {
  try {
    const response = await fetch(
        `http://localhost:8888/api/vaccine/statistic?yearSelected=${selectedYear}`);
    return await response.json();
  } catch (error) {
    console.error('Error fetching data:', error);
  }
}

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];
const color = "#17a2b8";
let label = "Dataset";
let type = "bar";

async function createChart() {
  let myData;
  const selectedYear = document.getElementById("year").value;
  const reportType = document.getElementById("reportAll").value;

  if (reportType === 'customer') {
    myData = await fetchCustomerReportData(selectedYear);
  }
  if (reportType === 'injectionResult') {
    myData = await fetchInjectionResultReportData(selectedYear);
  }
  if (reportType === 'vaccine') {
    myData = await fetchVaccineReportData(selectedYear);
  }
  let data = {
    labels: months,
    datasets: [
      {
        label: label,
        backgroundColor: color,
        borderColor: color,
        data: myData,
      },
    ],
  };
  let config = {
    type: type,
    data,
    options: {},
  };

  let myChart = new Chart(document.getElementById("myChart"), config);

  document.getElementById("mySelect").addEventListener("change", fun);

  document.getElementById("year").addEventListener("change", yearChange)

  function fun() {
    const value = document.getElementById("mySelect").value;
    if (value === "2") {
      config.type = "line";
      config.options = {
        animations: {
          tension: {
            duration: 1000,
            easing: 'linear',
            from: 1,
            to: 0,
            loop: true
          }
        }
      }
    } else {
      config.type = "bar";
    }

    myChart.destroy();
    myChart = new Chart(document.getElementById("myChart"), config);
  }

  async function yearChange() {
    const selectedYear = document.getElementById("year").value;
    let myData;
    if (reportType === 'customer') {
      myData = await fetchCustomerReportData(selectedYear);
    }
    if (reportType === 'injectionResult') {
      myData = await fetchInjectionResultReportData(selectedYear);
    }
    if (reportType === 'vaccine') {
      myData = await fetchVaccineReportData(selectedYear);
    }
    config.data.datasets[0].data = myData;
    myChart.destroy();
    myChart = new Chart(document.getElementById("myChart"), config);
  }
}

createChart();

